package util.graph;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Iterator;

/**
 * Implementation of graph algorithms for a (undirected) graph structure
 * Considering generic vertex V and edge E types
 *
 * Works on AdjancyMatrixGraph objects
 *
 * @author DEI-ESINF
 *
 */
public class GraphAlgorithms {

    private static <T> LinkedList<T> reverse(LinkedList<T> list) {
        LinkedList<T> reversed = new LinkedList<T>();
        Iterator<T> it = list.iterator();
        while (it.hasNext()) {
            reversed.push(it.next());
        }
        return reversed;
    }

    /**
     * Performs depth-first search of the graph starting at vertex. Calls
     * package recursive version of the method.
     *
     * @param graph Graph object
     * @param vertex Vertex of graph that will be the source of the search
     * @return queue of vertices found by search (including vertex), null if
     * vertex does not exist
     *
     */
    public static <V, E> LinkedList<V> DFS(AdjacencyMatrixGraph<V, E> graph, V vertex) {

        int index = graph.toIndex(vertex);
        if (index == -1) {
            return null;
        }

        LinkedList<V> resultQueue = new LinkedList<V>();
        resultQueue.add(vertex);
        boolean[] knownVertices = new boolean[graph.numVertices];
        DFS(graph, index, knownVertices, resultQueue);
        return resultQueue;
    }

    /**
     * Actual depth-first search of the graph starting at vertex. The method
     * adds discovered vertices (including vertex) to the queue of vertices
     *
     * @param graph Graph object
     * @param index Index of vertex of graph that will be the source of the
     * search
     * @param known previously discovered vertices
     * @param verticesQueue queue of vertices found by search
     *
     */
    static <V, E> void DFS(AdjacencyMatrixGraph<V, E> graph, int index, boolean[] knownVertices, LinkedList<V> verticesQueue) {
        knownVertices[index] = true;
        for (int i = 0; i < graph.numVertices; i++) {
            if (graph.edgeMatrix[index][i] != null && knownVertices[i] == false) {
                verticesQueue.add(graph.vertices.get(i));
                DFS(graph, i, knownVertices, verticesQueue);
            }
        }
    }

    /**
     * Performs breath-first search of the graph starting at vertex. The method
     * adds discovered vertices (including vertex) to the queue of vertices
     *
     * @param graph Graph object
     * @param vertex Vertex of graph that will be the source of the search
     * @return queue of vertices found by search (including vertex), null if
     * vertex does not exist
     *
     */
    public static <V, E> LinkedList<V> BFS(AdjacencyMatrixGraph<V, E> graph, V vertex) {
        if (graph.checkVertex(vertex)) {
            LinkedList<V> qbfs = new LinkedList<>();
            ArrayList<V> qaux = new ArrayList<>();
            qbfs.add(vertex);
            qaux.add(vertex);
            int[] checkVert = new int[graph.numVertices];
            checkVert[graph.toIndex(vertex)] = 1;
            while (!qaux.isEmpty()) {
                V vertaux = qaux.get(0);
                qaux.remove(vertaux);
                for (V vert : graph.directConnections(vertaux)) {
                    if (checkVert[graph.toIndex(vert)] == 0) {
                        qbfs.add(vert);
                        qaux.add(vert);
                        checkVert[graph.toIndex(vert)] = 1;
                    }
                }
            }
            return qbfs;
        } else {
            return null;
        }
    }

    /**
     * All paths between two vertices Calls recursive version of the method.
     *
     * @param graph Graph object
     * @param source Source vertex of path
     * @param dest Destination vertex of path
     * @param path LinkedList with paths (queues)
     * @return false if vertices not in the graph
     *
     */
    public static <V, E> boolean allPaths(AdjacencyMatrixGraph<V, E> graph, V source, V dest, LinkedList<LinkedList<V>> paths) {
        int index = graph.toIndex(source);
        int index2 = graph.toIndex(dest);
        if (index == -1 || index2 == -1) {
            return false;
        } else {
            paths.clear();
            boolean[] knownVertices = new boolean[graph.numVertices];
            for (int i = 0; i < knownVertices.length; i++) {
                knownVertices[i] = false;
            }
            LinkedList<V> auxStack = new LinkedList<>();
            allPaths(graph, index, index2, knownVertices, auxStack, paths);
            return true;
        }
    }

    /**
     * Actual paths search The method adds vertices to the current path (stack
     * of vertices) when destination is found, the current path is saved to the
     * list of paths
     *
     * @param graph Graph object
     * @param sourceIdx Index of source vertex
     * @param destIdx Index of destination vertex
     * @param knownVertices previously discovered vertices
     * @param auxStack stack of vertices in the path
     * @param path LinkedList with paths (queues)
     *
     */
    static <V, E> void allPaths(AdjacencyMatrixGraph<V, E> graph, int sourceIdx, int destIdx, boolean[] knownVertices, LinkedList<V> auxStack, LinkedList<LinkedList<V>> paths) {
        knownVertices[sourceIdx] = true;
        auxStack.push(graph.vertices.get(sourceIdx));
        for (V v : graph.directConnections(graph.vertices.get(sourceIdx))) {
            if (v == graph.vertices.get(destIdx)) {
                auxStack.push(v);
                paths.add(reverse(auxStack));
                auxStack.pop();
            } else {
                if (!knownVertices[graph.toIndex(v)]) {
                    allPaths(graph, graph.toIndex(v), destIdx, knownVertices, auxStack, paths);
                }
            }
        }
        knownVertices[sourceIdx] = false;
        auxStack.pop();
    }

    /**
     * Transforms a graph into its transitive closure uses the Floyd-Warshall
     * algorithm
     *
     * @param graph Graph object
     * @param dummyEdge object to insert in the newly created edges
     * @return the new graph
     */
    public static <V, E> AdjacencyMatrixGraph<V, E> transitiveClosure(AdjacencyMatrixGraph<V, E> graph, E dummyEdge) {
        AdjacencyMatrixGraph<V, E> graph2 = (AdjacencyMatrixGraph<V, E>) graph.clone();
        for (int k = 0; k < graph2.numVertices; k++) {
            for (int i = 0; i < graph2.numVertices; i++) {
                if (i != k && graph2.getEdge(graph2.vertices.get(i), graph2.vertices.get(k)) != null) {
                    for (int j = 0; j < graph2.numVertices; j++) {
                        if (i != j && j != k && graph2.getEdge(graph2.vertices.get(k), graph2.vertices.get(j)) != null) {
                            graph2.insertEdge(graph2.vertices.get(i), graph2.vertices.get(j), dummyEdge);
                        }
                    }
                }
            }
        }
        return graph2;
    }


}
