/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.util.LinkedList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import util.graph.AdjacencyMatrixGraph;

/**
 *
 * @author user
 */
public class RedeEstradasTest {

    public RedeEstradasTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of numLocais method, of class RedeEstradas.
     */
    @Test
    public void testNumLocais() {

        System.out.println("numLocais");
        RedeEstradas instance = new RedeEstradas();
        int expResult = 0;
        int result = instance.numLocais();
        assertEquals(expResult, result);

        instance.adicionarLocal(new Local("Local1", 20));
        instance.adicionarLocal(new Local("Local2", 30));
        expResult = 2;
        result = instance.numLocais();

        assertEquals(expResult, result);

    }

    /**
     * Test of numEstradas method, of class RedeEstradas.
     */
    @Test
    public void testNumEstradas() {

        System.out.println("numEstradas");
        RedeEstradas instance = new RedeEstradas();
        int expResult = 0;
        int result = instance.numEstradas();
        assertEquals(expResult, result);

        Local local1 = new Local("Local1", 20);
        Local local2 = new Local("Local2", 30);
        Local local3 = new Local("Local3", 50);
        instance.adicionarLocal(local1);
        instance.adicionarLocal(local2);
        instance.adicionarLocal(local3);
        instance.adicionarEstrada(local1, local2, 20);
        instance.adicionarEstrada(local2, local3, 50);
        expResult = 2;
        result = instance.numEstradas();

        assertEquals(expResult, result);

    }

    /**
     * Test of adicionarLocal method, of class RedeEstradas.
     */
    @Test
    public void testAdicionarLocal() {

        System.out.println("insertLocal");
        Local local = new Local("Local1", 20);
        RedeEstradas instance = new RedeEstradas();
        boolean expResult = true;
        boolean result = instance.adicionarLocal(local);
        assertEquals(expResult, result);

    }

    /**
     * Test of adicionarEstrada method, of class RedeEstradas.
     */
    @Test
    public void testAdicionarEstrada() {

        System.out.println("insertEstrada");
        Local from = new Local("Local1", 20);
        Local to = new Local("Local2", 30);
        int nPontos = 30;
        RedeEstradas instance = new RedeEstradas();
        instance.adicionarLocal(from);
        instance.adicionarLocal(to);
        boolean expResult = true;
        boolean result = instance.adicionarEstrada(from, to, nPontos);
        assertEquals(expResult, result);
    }

    /**
     * Test of estradasDeUmLocal method, of class RedeEstradas.
     */
    @Test
    public void testEstradasDeUmLocal() {

        System.out.println("departingEstradas");
        Local local1 = new Local("Local1", 20);
        Local local2 = new Local("Local2", 30);
        RedeEstradas instance = new RedeEstradas();
        instance.adicionarLocal(local1);
        instance.adicionarLocal(local2);
        instance.adicionarEstrada(local1, local2, 50);
        LinkedList<Estrada> expResult = new LinkedList<>();
        expResult.add(new Estrada(50));
        Iterable<Estrada> result = instance.estradasDeUmLocal(local1);

        assertEquals(expResult, result);
    }

    /**
     * Test of locaisDiretamenteConectados method, of class RedeEstradas.
     */
    @Test
    public void testLocaisDiretamenteConectados() {

        System.out.println("locaisDiretamenteConectados");

        Personagem p1 = new Personagem("Pers1", 22, new Local());
        Personagem p2 = new Personagem("Pers2", 22, new Local());
        RedeEstradas instance = new RedeEstradas();

        Local l1 = new Local("Local1", p1, 22);
        Local l2 = new Local("Local2", p2, 22);
        Local l3 = new Local("Local3", p1, 22);
        Local l4 = new Local("Local4", p2, 22);
        instance.adicionarLocal(l1);
        instance.adicionarLocal(l2);
        instance.adicionarLocal(l3);
        instance.adicionarLocal(l4);
        instance.adicionarEstrada(l2, l4, 22);
        instance.adicionarEstrada(l1, l3, 22);
        instance.adicionarEstrada(l3, l4, 22);

        LinkedList<Local> expResultl1 = new LinkedList<>();
        LinkedList<Local> resultl1 = new LinkedList<>();
        expResultl1.add(l3);

        Iterable<Local> instanceResultl1 = instance.locaisDiretamenteConectados(l1);
        for (Local l : instanceResultl1) {
            resultl1.add(l);
        }
        assertEquals(expResultl1, resultl1);

        LinkedList<Local> expResultl4 = new LinkedList<>();
        LinkedList<Local> resultl4 = new LinkedList<>();
        expResultl4.add(l2);
        expResultl4.add(l3);
        Iterable<Local> instanceResultl4 = instance.locaisDiretamenteConectados(l4);
        for (Local l : instanceResultl4) {
            resultl4.add(l);
        }
        assertEquals(expResultl4, resultl4);

    }

    /**
     * Test of verificarConexao method, of class RedeEstradas.
     */
    @Test
    public void testVerificarConexao() {

        Personagem p1 = new Personagem("Pers1", 22, new Local());
        Personagem p2 = new Personagem("Pers2", 22, new Local());
        RedeEstradas instance = new RedeEstradas();

        Local l1 = new Local("Local1", p1, 22);
        Local l2 = new Local("Local2", p2, 22);
        Local l3 = new Local("Local3", p1, 22);
        Local l4 = new Local("Local4", p2, 22);
        instance.adicionarLocal(l1);
        instance.adicionarLocal(l2);
        instance.adicionarLocal(l3);
        instance.adicionarLocal(l4);
        instance.adicionarEstrada(l2, l4, 22);
        instance.adicionarEstrada(l1, l3, 25);
        instance.adicionarEstrada(l3, l4, 26);

        LinkedList<Local> expResultl1 = new LinkedList<>();

        expResultl1.add(l1);
        expResultl1.add(l3);

        LinkedList<Local> resultl1 = instance.verificarConexao(l1, l3);

        assertEquals(expResultl1, resultl1);

    }

    /**
     * Test of irPeloNumeroMinimoDeEstradas method, of class RedeEstradas.
     */
    @Test
    public void testIrPeloNumeroMinimoDeEstradas() {

        System.out.println("irPeloNumeroMinimoDeEstradas");
        RedeEstradas instance = new RedeEstradas();
        Local local1 = new Local("local1", 20);
        Local local2 = new Local("local2", 30);
        Local local3 = new Local("local3", 50);
        instance.adicionarLocal(local1);
        instance.adicionarLocal(local2);
        instance.adicionarLocal(local3);
        instance.adicionarEstrada(local1, local2, 20);
        instance.adicionarEstrada(local2, local3, 50);
        LinkedList<Local> expResult = new LinkedList<>();
        expResult.add(local1);
        expResult.add(local2);
        expResult.add(local3);
        Iterable<Local> result = instance.irPeloNumeroMinimoDeEstradas(local1, local3);

        assertEquals(expResult, result);

    }

    /**
     * Test of cloneDificuldade method, of class RedeEstradas.
     */
    @Test
    public void testCloneDificuldade() {

        System.out.println("cloneDificuldade");
        RedeEstradas instance = new RedeEstradas();
        Local local1 = new Local("local1", 20);
        Local local2 = new Local("local2", 30);
        Local local3 = new Local("local3", 50);
        instance.adicionarLocal(local1);
        instance.adicionarLocal(local2);
        instance.adicionarLocal(local3);
        instance.adicionarEstrada(local1, local2, 20);
        instance.adicionarEstrada(local2, local3, 50);
        AdjacencyMatrixGraph<Local, Integer> expResult = new AdjacencyMatrixGraph<>();
        expResult.insertVertex(local1);
        expResult.insertVertex(local2);
        expResult.insertVertex(local3);
        expResult.insertEdge(local1, local2, 20);
        expResult.insertEdge(local2, local3, 50);
        AdjacencyMatrixGraph<Local, Integer> result = instance.cloneDificuldade();

        assertEquals(expResult, result);
    }

    /**
     * Test of getLocalPeloNome method, of class RedeEstradas.
     */
    @Test
    public void testGetLocalPeloNome() {

        System.out.println("getLocalPeloNome");
        String nome = "Local1";
        RedeEstradas instance = new RedeEstradas();
        Local expResult = new Local("Local1", 20);
        instance.adicionarLocal(expResult);
        Local result = instance.getLocalPeloNome(nome);
        assertEquals(expResult, result);

    }

    /**
     * Test of dificuldadeMinima method, of class RedeEstradas.
     */
    @Test
    public void testDificuldadeMinima() {

        System.out.println("dificuldadeMinima");
        RedeEstradas instance = new RedeEstradas();
        Local local1 = new Local("local1", 20);
        Local local2 = new Local("local2", 30);
        Local local3 = new Local("local3", 50);
        instance.adicionarLocal(local1);
        instance.adicionarLocal(local2);
        instance.adicionarLocal(local3);
        instance.adicionarEstrada(local1, local2, 20);
        instance.adicionarEstrada(local2, local3, 50);
        LinkedList<Local> path = new LinkedList<>();
        double expResult = 70;
        double result = instance.dificuldadeMinima(local1, local3, path);

        assertEquals(expResult, result, 0.0);

    }

    /**
     * Test of adicionarLocalAPersonagemAlianca method, of class RedeEstradas.
     */
    @Test
    public void testAdicionarLocalAPersonagemAlianca() {

        System.out.println("adicionarLocalAPersonagemAlianca");
        Local local1 = new Local("local1", 20);
        Local local2 = new Local("local2", 40);
        Local local3 = new Local("local3", 10);
        RedeEstradas instance = new RedeEstradas();
        instance.adicionarLocal(local1);
        instance.adicionarLocal(local2);
        instance.adicionarLocal(local3);
        instance.adicionarEstrada(local1, local2, 40);
        instance.adicionarEstrada(local2, local3, 20);

        Personagem personagemPrincipal = new Personagem("Jose", 50, local1);
        Personagem aliado = new Personagem("Maria", 70, local2);
        local1.setP(personagemPrincipal);
        local2.setP(aliado);

        RedeAliancas redeDeAliancas = new RedeAliancas();
        redeDeAliancas.adicionarPersonagem(personagemPrincipal);
        redeDeAliancas.adicionarPersonagem(aliado);
        redeDeAliancas.adicionarAlianca(true, 0.8f, personagemPrincipal, aliado);

        boolean expResult = true;
        boolean result = instance.adicionarLocalAPersonagemAlianca(local3, personagemPrincipal, aliado, redeDeAliancas);

        assertEquals(expResult, result);

    }

    /**
     * Test of adicionarLocalAPersonagem method, of class RedeEstradas.
     */
    @Test
    public void testAdicionarLocalAPersonagem() {

        System.out.println("AdicionarLocalAPersonagemAlianca");
        Local local1 = new Local("local1", 20);
        Local local2 = new Local("local2", 40);
        Local local3 = new Local("local3", 10);
        RedeEstradas instance = new RedeEstradas();
        instance.adicionarLocal(local1);
        instance.adicionarLocal(local2);
        instance.adicionarLocal(local3);
        instance.adicionarEstrada(local1, local2, 40);
        instance.adicionarEstrada(local2, local3, 20);

        Personagem personagemPrincipal = new Personagem("Jose", 50, local1);
        Personagem aliado = new Personagem("Maria", 70, local2);
        local1.setP(personagemPrincipal);
        local2.setP(aliado);

        RedeAliancas redeAliancas = new RedeAliancas();
        redeAliancas.adicionarPersonagem(personagemPrincipal);
        redeAliancas.adicionarPersonagem(aliado);
        redeAliancas.adicionarAlianca(true, 0.8f, personagemPrincipal, aliado);

        boolean expResult = true;
        boolean result = instance.adicionarLocalAPersonagemAlianca(local3, personagemPrincipal, aliado, redeAliancas);

        assertEquals(expResult, result);
    }

    /**
     * Test of getForcaNecessariaParaConquistarLocal method, of class
     * RedeEstradas.
     */
    @Test
    public void testGetForcaNecessariaParaConquistarLocal() {

        System.out.println("getForcaNecessariaParaConquistarLocal");
        Local local1 = new Local("local1", 20);
        Personagem personagem = new Personagem("Jose", 70, local1);
        RedeAliancas redeDeAliancas = new RedeAliancas();

        Local local2 = new Local("local2", 20);
        Local local3 = new Local("local3", 20);
        Local local4 = new Local("local4", 20);
        RedeEstradas instance = new RedeEstradas();
        instance.adicionarLocal(local1);
        instance.adicionarLocal(local2);
        instance.adicionarLocal(local3);
        instance.adicionarLocal(local4);
        instance.adicionarEstrada(local1, local2, 20);
        instance.adicionarEstrada(local2, local3, 5);
        instance.adicionarEstrada(local3, local4, 9);
        int expResult = 94;
        int result = instance.getForcaNecessariaParaConquistarLocal(local4, personagem, redeDeAliancas);

        assertEquals(expResult, result);

    }

    /**
     * Test of caminhoMaisCurtoEntrePersonagemELocalDesejado method, of class
     * RedeEstradas.
     */
    @Test
    public void testCaminhoMaisCurtoEntrePersonagemELocalDesejado() {

        System.out.println("CaminhoMaisCurtoEntrePersonagemELocalDesejado");
        Local local1 = new Local("local1", 20);
        Personagem personagem = new Personagem("Jose", 70, local1);
        Local local2 = new Local("local2", 3);
        Local local3 = new Local("local3", 6);
        Local local4 = new Local("local4", 3);
        Local local5 = new Local("local5", 3);
        Local local6 = new Local("local6", 3);
        RedeEstradas instance = new RedeEstradas();
        instance.adicionarLocal(local1);
        instance.adicionarLocal(local2);
        instance.adicionarLocal(local3);
        instance.adicionarLocal(local4);
        instance.adicionarLocal(local5);
        instance.adicionarLocal(local6);
        instance.adicionarEstrada(local1, local2, 3);
        instance.adicionarEstrada(local2, local3, 3);
        instance.adicionarEstrada(local3, local4, 3);
        instance.adicionarEstrada(local2, local5, 3);
        instance.adicionarEstrada(local5, local6, 3);
        instance.adicionarEstrada(local5, local4, 3);
        List<Local> expResult = new LinkedList<>();
        expResult.add(local1);
        expResult.add(local2);
        expResult.add(local3);
        expResult.add(local4);
        List<Local> result = instance.caminhoMaisCurtoEntrePersonagemELocalDesejado(local4, personagem);

        assertEquals(expResult, result);
    }

    /**
     * Test of defenderLocalAlianca method, of class RedeEstradas.
     */
    @Test
    public void testDefenderLocalAlianca() {

        System.out.println("defenderLocalByAlianca");
        Local local1 = new Local("local1", 20);
        Personagem personagemAtacante = new Personagem("pA", 50, local1);
        local1.setP(personagemAtacante);

        Local local2 = new Local("local2", 20);
        Personagem aliado = new Personagem("pAliado", 50, local2);
        local2.setP(aliado);

        Local local3 = new Local("local3", 30);
        Personagem defensor = new Personagem("pDefensor", 20, local3);
        local3.setP(defensor);

        RedeAliancas redeAliancas = new RedeAliancas();
        redeAliancas.adicionarPersonagem(aliado);
        redeAliancas.adicionarPersonagem(personagemAtacante);
        redeAliancas.adicionarPersonagem(defensor);
        redeAliancas.adicionarAlianca(true, 0.8f, defensor, aliado);

        RedeEstradas instance = new RedeEstradas();
        instance.adicionarLocal(local1);
        instance.adicionarLocal(local2);
        instance.adicionarLocal(local3);
        instance.adicionarEstrada(local3, local2, 20);
        instance.adicionarEstrada(local1, local2, 30);

        boolean expResult = false;
        boolean result = instance.defenderLocalAlianca(local3, personagemAtacante, aliado, redeAliancas);

        assertEquals(expResult, result);
    }

    /**
     * Test of podePersonagemConquistarLocal method, of class RedeEstradas.
     */
    @Test
    public void testPodePersonagemConquistarLocal() {

        System.out.println("podePersonagemConquistarLocal");
        Local local1 = new Local("local1", 20);
        Personagem personagem = new Personagem("Maria", 70, local1);
 
        Float forcaDeDefesa = 20f;
        RedeEstradas instance = new RedeEstradas();
        instance.adicionarLocal(local1);
        Local local2 = new Local("local2", 20);
        instance.adicionarLocal(local2);
        instance.adicionarEstrada(local1, local2, 20);

        Float expResult = 60f;
        Float result = instance.podePersonagemConquistarLocal(local2, personagem, forcaDeDefesa);

        assertEquals(expResult, result);
    }

    /**
     * Test of getPodePersonagemConquistarLocal method, of class RedeEstradas.
     */
    @Test
    public void testGetPodePersonagemConquistarLocal() {

        System.out.println("getPodePersonagemConquistarLocal");
        Local local1 = new Local("local1", 20);
        Personagem personagem = new Personagem("maria", 70, local1);

        RedeAliancas redeAliancas = new RedeAliancas();
        redeAliancas.adicionarPersonagem(personagem);

        RedeEstradas instance = new RedeEstradas();
        instance.adicionarLocal(local1);
        Local local2 = new Local("local2", 15);
        Local local3 = new Local("local3", 5);
        instance.adicionarLocal(local2);
        instance.adicionarLocal(local3);
        instance.adicionarEstrada(local1, local2, 10);
        instance.adicionarEstrada(local2, local3, 10);

        Boolean expResult = true;
        Boolean result = instance.getPodePersonagemConquistarLocal(local3, personagem, redeAliancas);

        assertEquals(expResult, result);
    }

}
