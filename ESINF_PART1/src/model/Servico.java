package model;

/**
 *
 * @author i160901
 */
public class Servico {
    
    private char codigo;
    
    private static final char CODIGO_OMISSAO = ' ';
    
    
    
    public Servico() {
        codigo = CODIGO_OMISSAO;
    }
    
    public Servico(char codigo) {
        this.codigo = codigo;
    }

    
    
    
    public char getCodigo() {
        return codigo;
    }

    public void setCodigo(char codigo) {
        this.codigo = codigo;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 47 * hash + this.codigo;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Servico other = (Servico) obj;
        if (this.codigo != other.codigo) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Servico{" + "codigo=" + codigo + '}';
    }   
}
            
    

