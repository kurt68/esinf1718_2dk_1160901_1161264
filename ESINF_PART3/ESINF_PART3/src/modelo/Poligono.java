/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.util.Objects;

/**
 *
 * @author user
 */
public class Poligono implements Comparable<Poligono> {

    private Integer lados;
    private String nome;
    
    private static final Integer LADOS_OMISSAO = 0;
    private static final String NOME_OMISSAO = "sem_nome";

    //construtor
    public Poligono(Integer lados, String nome) {
        this.lados = lados;
        this.nome = nome;
    }
    
    public Poligono(){
        lados= LADOS_OMISSAO;
        nome= NOME_OMISSAO;
    }

    //getter
    public Integer getLados() {
        return lados;
    }

    public String getNome() {
        return nome;
    }

    //setter
    public void setLados(Integer lados) {
        this.lados = lados;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 23 * hash + Objects.hashCode(this.lados);
        hash = 23 * hash + Objects.hashCode(this.nome);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Poligono other = (Poligono) obj;
        if (!Objects.equals(this.nome, other.nome)) {
            return false;
        }
        if (!Objects.equals(this.lados, other.lados)) {
            return false;
        }
        return true;
    }

    @Override
    public int compareTo(Poligono p) {
        return lados.compareTo(p.getLados());
    }

    @Override
    public String toString() {
        return "Poligono{" + "lados=" + lados + ", nome=" + nome + '}';
    }

}
