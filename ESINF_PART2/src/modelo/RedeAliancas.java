/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import util.graphbase.Edge;
import util.graphbase.Graph;
import util.graphbase.GraphAlgorithms;

/**
 *
 * @author user
 */
public class RedeAliancas {

    private Graph<Personagem, Alianca> redeAliancas;

    public RedeAliancas() {
        redeAliancas = new Graph<>(false);
    }

    public boolean adicionarPersonagem(Personagem p) {
        return (redeAliancas.insertVertex(p));
    }

    public boolean adicionarAlianca(boolean tipo, Float fatorCompatibilidade, Personagem p1, Personagem p2) {
        return (redeAliancas.insertEdge(p1, p2, new Alianca(tipo, fatorCompatibilidade, p1, p2), ((p1.getForca() + p2.getForca()) * fatorCompatibilidade)));
    }

    public int numPersonagens() {
        return redeAliancas.numVertices();
    }

    public int numAliancas() {
        return redeAliancas.numEdges();
    }

    public Personagem getPersonagemPeloNome(String nome) {

        Iterator<Personagem> personagens = redeAliancas.vertices().iterator();
        Personagem personagem = null;

        while (personagens.hasNext()) {

            Personagem personagemAtual = personagens.next();
            String nomePersonagemAtual = personagemAtual.getNome();

            if (nomePersonagemAtual.equals(nome)) {
                personagem = personagemAtual;
            }
        }
        return personagem;
    }

    public List<Alianca> getAliancas() {

        List<Alianca> listaAliancas = new LinkedList<>();

        for (Personagem p : redeAliancas.vertices()) {
            for (Personagem outra : redeAliancas.vertices()) {
                if (!p.equals(outra)) {

                    Edge<Personagem, Alianca> edge = redeAliancas.getEdge(p, outra);
                    if (edge != null) {
                        Alianca a = edge.getElement();
                        if (a != null && !listaAliancas.contains(a)) {
                            listaAliancas.add(a);
                        }
                    }
                }
            }
        }
        return listaAliancas;
    }

    public List<Personagem> getAliadosAUmaPersonagem(Personagem p) {

        List<Personagem> aliados = new LinkedList<>();
        for (Personagem personagem : redeAliancas.vertices()) {
            if (!(personagem.equals(p))) {
                if (redeAliancas.getEdge(personagem, p) != null) {
                    aliados.add(personagem);
                }

            }
        }

        return aliados;
    }

    public Iterable<Personagem> irPeloNumMinimoAliancas(Personagem p1, Personagem p2) {

        //verificar se elas existem
        if (!redeAliancas.validVertex(p1) || !redeAliancas.validVertex(p2)) {
            return null;
        }

        LinkedList<LinkedList<Personagem>> caminhos = new LinkedList<>();
        GraphAlgorithms.allPaths(redeAliancas, p1, p2);
        LinkedList<Personagem> returnList = new LinkedList<>();
        double i = 0;

        for (LinkedList<Personagem> lista : caminhos) {
            if (caminhos.indexOf(lista) == 0 || GraphAlgorithms.shortestPath(redeAliancas, p1, p2, lista) < i) {
                i = GraphAlgorithms.shortestPath(redeAliancas, p1, p2, lista);
                returnList = lista;
            }
        }

        return returnList;
    }

    public boolean localPertenceAliados(Local l, Personagem p) {

        if (!getAliadosAUmaPersonagem(p).isEmpty()) {
            for (Personagem personagem : getAliadosAUmaPersonagem(p)) {
                if (l.getP() != null && l.getP().equals(personagem)) {
                    return true;
                }
            }
        }

        //se nao se verificar, nao pertence, ou seja 'false'
        return false;
    }

    public float keyPersonagem(Personagem p) {
        return redeAliancas.getKey(p);
    }

    public Personagem personagemByKey(int key) {
        Personagem[] result = redeAliancas.allkeyVerts();
        return result[key];
    }

    public Float forcaPersonagens(Personagem p1, Personagem p2) {
        return redeAliancas.getEdge(p1, p2).getWeight();
    }

    public String aliancasPersonagens() {

        String redeAliancasString = "";
        for (Personagem p : redeAliancas.vertices()) {
            int f = redeAliancas.inDegree(p) + redeAliancas.outDegree(p);
            redeAliancasString = "Nome " + p.getNome() + " " + " forca " + f + "\n";
        }

        return redeAliancasString;
    }

    public List<Personagem> redeAliancasForcaMaxima() {

        List<Personagem> max = new LinkedList<>();

        for (Personagem p : redeAliancas.vertices()) {
            for (Personagem p2 : redeAliancas.vertices()) {
                if (p2 != null) {
                    if (max.isEmpty()) {
                        max.add(p);
                        max.add(p2);
                    } else {
                        Float forcaDasPersonagens = forcaPersonagens(p, p2);
                        if (forcaDasPersonagens != null && forcaDasPersonagens > forcaPersonagens(max.get(0), max.get(1)));

                        max.clear();;
                        max.add(p);
                        max.add(p2);
                    }
                }
            }
        }
        
        return max;
    }
    
    public RedeAliancas aliancasPublicas() {
        
        RedeAliancas nova = new RedeAliancas();
        
        for(Personagem p : redeAliancas.vertices()) {
            nova.adicionarPersonagem(p);
        }
        for(Personagem p : redeAliancas.vertices()) {
            for( Personagem p2 : redeAliancas.vertices()) {
                if(!p.equals(p2)) {
                    nova.adicionarAlianca(true, 1f, p, p2);
                }
            }
        }
        
        return nova;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 53 * hash + Objects.hashCode(this.redeAliancas);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final RedeAliancas other = (RedeAliancas) obj;
        if (!Objects.equals(this.redeAliancas, other.redeAliancas)) {
            return false;
        }
        return true;
    }
    
    @Override
    public String toString() {
        return redeAliancas.toString();
    }

}
