package model;

import java.util.Objects;
import util.DoublyLinkedList;

/**
 *
 * @author user
 */
public class Cidadao {
    
    //atributos
    private int nif;
    private String nome;
    private String email;
    private String codPostal;
    private int numReparticao;
   
    
    //variaveis omissao
    private final int NIF_OMISSAO = 000000000;
    private final String NOME_OMISSAO = "Nome";
    private final String EMAIL_OMISSAO = "aaa@gmail.com";
    private final String COD_POSTAL_OMISSAO = "0000-000";
    private final int NUM_REPARTICAO_OMISSAO = 0;
    private DoublyLinkedList<Senha> senhas;
    
    
    //construtores
    public Cidadao() {
        nif = NIF_OMISSAO;
        nome = NOME_OMISSAO;
        email = EMAIL_OMISSAO;
        codPostal = COD_POSTAL_OMISSAO;
        numReparticao = NUM_REPARTICAO_OMISSAO;
        senhas = new DoublyLinkedList<Senha>();
    }
    
    public Cidadao(int nif, String nome, String email, String codPostal, int numReparticao) {
        this.nif = nif;
        this.nome = nome;
        this.email = email;
        this.codPostal = codPostal;
        this.numReparticao = numReparticao;
        senhas = new DoublyLinkedList<Senha>();
    }

    public int getNif() {
        return nif;
    }

    public String getNome() {
        return nome;
    }
    
    public String getEmail() {
        return email;
    }

    public String getCodPostal() {
        return codPostal;
    }
    
    public int getNumReparticao() {
        return numReparticao;
    }

    public DoublyLinkedList<Senha> getSenhas() {
        return senhas;
    }
    

    public void setNif(int nif) {
        this.nif = nif;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setCodPostal(String codPostal) {
        this.codPostal = codPostal;
    }
    
    public void setNumReparticao (int numReparticao) {
        this.codPostal = codPostal;
    }

    public void setSenhas(DoublyLinkedList<Senha> senhas) {
        this.senhas = senhas;
    }
    
    

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 41 * hash + this.nif;
        hash = 41 * hash + Objects.hashCode(this.nome);
        hash = 41 * hash + Objects.hashCode(this.email);
        hash = 41 * hash + Objects.hashCode(this.codPostal);
        hash = 41 * hash + this.numReparticao;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Cidadao other = (Cidadao) obj;
        if (this.nif != other.nif) {
            return false;
        }
        if (this.numReparticao != other.numReparticao) {
            return false;
        }
        if (!Objects.equals(this.nome, other.nome)) {
            return false;
        }
        if (!Objects.equals(this.email, other.email)) {
            return false;
        }
        if (!Objects.equals(this.codPostal, other.codPostal)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Cidadao{" + "nif=" + nif + ", nome=" + nome + ", email=" + email + ", codPostal=" + codPostal + ", numReparticao=" + numReparticao + '}';
    }  
    
}
