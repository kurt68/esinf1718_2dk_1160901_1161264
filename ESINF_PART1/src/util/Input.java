package util;

import model.Cidadao;
import model.Reparticao;
import model.Senha;
import model.Servico;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import registos.RegistoReparticoes;

/**
 *
 * @author i1160901
 */
public class Input {

    public static boolean leituraReparticao(String file, RegistoReparticoes r) throws FileNotFoundException {

        Scanner in = new Scanner(new File(file));
        while (in.hasNext()) {
            String ler = in.nextLine();
            String[] linhaEmArray = ler.trim().split(",");
            if (linhaEmArray.length <= 0) {
                return false;
            }else{    
                DoublyLinkedList<Servico> servicos = new DoublyLinkedList();
                for (int i = 3; i < linhaEmArray.length; i++) {
                    Servico s = new Servico(linhaEmArray[i].charAt(0));
                    servicos.addLast(s);
                }
                Reparticao reparticaoNova = new Reparticao(linhaEmArray[0], linhaEmArray[2], Integer.parseInt(linhaEmArray[1]), servicos);
                r.addReparticao(reparticaoNova);
            }
        }
        return true;
    }

    public static Map<Cidadao, Set<Senha>> leituraSenhas(String file,String file2) throws FileNotFoundException {
        Map<Cidadao, Set<Senha>> cidadaoPorSenhas = new HashMap<>();
        Set<Senha> senhas = new HashSet<>();
        Scanner in = new Scanner(new File(file));
        while (in.hasNext()) {
            String ler = in.nextLine();
            String[] linhaEmArray = ler.trim().split(",");
            String nif = linhaEmArray[0];
            int numero = Integer.valueOf(linhaEmArray[2]);
            char[] servico = linhaEmArray[1].toCharArray();
            Senha s = new Senha(numero, servico[0]);
            senhas.add(s);
            Set<Cidadao> c = leituraCidadao(file2);
            for (Cidadao cid : c) {
                if (cid.getNif() == Integer.parseInt(nif)) {
                    cidadaoPorSenhas.put(cid, senhas);
                }
            }

        }
        return cidadaoPorSenhas;
    }

    public static Set<Cidadao> leituraCidadao(String file) throws FileNotFoundException {

        Set<Cidadao> cidadao = new HashSet<>();
        Scanner in = new Scanner(new File(file));
        while (in.hasNext()) {
            String ler = in.nextLine();
            String[] linhaEmArray = ler.trim().split(",");

            Cidadao c = new Cidadao(Integer.parseInt(linhaEmArray[1]), linhaEmArray[0], linhaEmArray[2], linhaEmArray[3], Integer.parseInt(linhaEmArray[4]));
            cidadao.add(c);

        }
        return cidadao;
    }

}
