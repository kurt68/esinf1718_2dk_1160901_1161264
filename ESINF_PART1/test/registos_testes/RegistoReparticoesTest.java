package registos_testes;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import model.*;
import static org.junit.Assert.assertTrue;
import org.junit.Test;
import registos.*;
import util.DoublyLinkedList;

/**
 *
 * @author user
 */
public class RegistoReparticoesTest {
    
    @Test 
    public void addReparticaoTest() throws Exception {
       System.out.println("Adiciona Reparticao");
       RegistoReparticoes registo = new RegistoReparticoes();
       Reparticao reparticao = new Reparticao("Vila das Aves", "4795", 1234, new DoublyLinkedList());
       
       assertTrue("Tem que retornar true",registo.addReparticao(reparticao)==true);
       assertTrue("Tem que retornar que tem uma reparticao",registo.getReparticoes().size()==1);
        
    }
    
    @Test 
    public void removeReparticaoTest() throws Exception {
        System.out.println("Remove Reparticao");
        RegistoReparticoes registo = new RegistoReparticoes();
        Reparticao reparticao = new Reparticao("Vila das Aves", "4795", 1234, new DoublyLinkedList());
        registo.addReparticao(reparticao);
        assertTrue("Tem que retornar true",registo.removeReparticao(reparticao)==true);
        assertTrue("Tem que retornar 0",registo.getReparticoes().size()==0);
    }
    
    @Test 
    public void getCidadaosReparticaoTest() throws Exception {
        System.out.println("Cidadao por reparticao");
        Cidadao c1 = new Cidadao(111222333,"Rui","rui@gmail.com","4795-003",1234);
        Cidadao c2 = new Cidadao(222333444,"Andre","andre@gmail.com","4795-999",1034);
        Cidadao c3 = new Cidadao(333444555,"Ze","ze@gmail.com","4795-002",1234);
        DoublyLinkedList<Cidadao> cidadaos = new DoublyLinkedList();
        DoublyLinkedList<Servico> serv = new DoublyLinkedList();
        serv.addLast(new Servico('A'));
        serv.addLast(new Servico('B'));
        serv.addLast(new Servico('C'));
        RegistoReparticoes registo = new RegistoReparticoes();
        Reparticao reparticao = new Reparticao("Vila das Aves", "4795", 1234, serv ,cidadaos);
        registo.addReparticao(reparticao);
        reparticao.getListaCidadaos().addLast(c1);
        reparticao.getListaCidadaos().addLast(c2);
        reparticao.getListaCidadaos().addLast(c3);
        Map<String, Set<Integer>> cidadaosDaReparticao = new HashMap<>();
        Set<Integer> cid = new HashSet<>();
        cid.add(c3.getNif());
        cid.add(c2.getNif());
        cid.add(c1.getNif());
        cidadaosDaReparticao.put("cidade: "+ reparticao.getCidade() + " ,numero de reparticao: " + reparticao.getNum(), cid);
        Map<String, Set<Integer>> teste =  registo.getCidadaosReparticao();
        assertTrue("Tem que retornar true",cidadaosDaReparticao.equals(registo.getCidadaosReparticao()));
    }
    

}
