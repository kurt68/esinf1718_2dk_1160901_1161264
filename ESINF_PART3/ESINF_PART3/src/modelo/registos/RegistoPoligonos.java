package modelo.registos;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javafx.scene.Node;
import modelo.Poligono;
import utils.AVL;
import utils.Input;
import utils.Arvore;

/**
 *
 * @author user
 */
public class RegistoPoligonos {

    private AVL<Poligono> unidades;
    private AVL<Poligono> dezenas;
    private AVL<Poligono> centenas;

    private static final String FICH_UNIDADES = "poligonos_prefixo_unidades.txt";
    private static final String FICH_DEZENAS = "poligonos_prefixo_dezenas.txt";
    private static final String FICH_CENTENAS = "poligonos_prefixo_centenas.txt";
    private static final String SUFIXO = "gon";

    // a)  criar arvores 
    public RegistoPoligonos() throws FileNotFoundException {

        this.unidades = Input.lerFicheiro(FICH_UNIDADES);
        this.dezenas = Input.lerFicheiro(FICH_DEZENAS);
        this.centenas = Input.lerFicheiro(FICH_CENTENAS);
    }

    // b) nome atraves dos lados
    public String getNomePelosLados(Integer lados) {

        String nome = "";
        ArrayList<Integer> aux = new ArrayList<>();

        //verifica se o numero de lados é inválido
        if (lados <= 0 || lados > 999) {
            return null;
        }

        //separa os algarismos do num de lados, guarda num ArrayList
        while (lados > 0) {
            aux.add(lados % 10);
            lados = lados / 10;
        }

        if (aux.size() <= 3) {  //se o num de algarismos for menor ou igual a 3,

            if (aux.size() == 3) {  // se for 3

                if (aux.get(2) > 0) {
                    Integer centenasNum = aux.get(2) * 100; // ele vai buscar o nome correspondente das centenas
                    nome += getPoligonoPelosLados(centenasNum).getNome();

                }
            }
            if (aux.size() >= 2) {  //se for maior ou igual a 2

                if (aux.get(1) > 0) {

                    Integer check = (aux.get(1) * 10) + aux.get(0);

                    if (check < 30 && check > 0) {   //ate 30 o nome nao é constante, conforme o ficheiro

                        nome += getPoligonoPelosLados(check).getNome();
                        return nome + SUFIXO;

                    } else {  //a partir de 30 é so formar palavras com os nomes

                        Integer dezenasNum = aux.get(1) * 10;
                        nome += getPoligonoPelosLados(dezenasNum).getNome(); //ele vai buscar o nome correspondente das dezenas

                    }
                }
            }
            if (aux.size() >= 1) {

                Integer unidadesNum = aux.get(0);  //por fim, ele vai buscar a ultima parte do nome, com as unidades

                if (unidadesNum > 0) {

                    nome += getPoligonoPelosLados(unidadesNum).getNome();
                }
            }
            return nome + SUFIXO; //retorna o nome construido ate agora, mais o sufixo "gon".

        } else {

            return null;
        }
    }

    public Poligono getPoligonoPelosLados(Integer lados) {

        Map<Integer, List<Poligono>> mapa;

        if (lados < 10) {
            mapa = unidades.nodesByLevel();
        } else if (lados < 100) {
            mapa = dezenas.nodesByLevel();
        } else {
            mapa = centenas.nodesByLevel();
        }

        if (mapa != null) {
            for (int i = 0; i < mapa.size(); i++) {

                List<Poligono> listaPoligonos = mapa.get(i);

                for (Poligono poligono : listaPoligonos) {

                    if (poligono.getLados().equals(lados)) {
                        return poligono;
                    }
                }
            }
        }
        return null;
    }

    // c) Com as três árvores anteriores, construir árvore balanceada com todos os nomes de polígonos regulares de 1 até 999 lados.
    public AVL<Poligono> getArvoreBalanceada() {

        AVL<Poligono> arvoreCompleta = new AVL<>();
        boolean flag;

        for (int i = 1; i < 1000; i++) {

            Poligono novo = new Poligono(i, getNomePelosLados(i));
            flag = isPoligonoRegular(novo);

            if (flag) {

                arvoreCompleta.insert(novo);
            }
        }
        return arvoreCompleta;
    }

    public boolean isPoligonoRegular(Poligono p) {
        // é regular a partir de 3 lados, segundo as informações que temos do enunciado
        return (p.getLados() >= 3 && p.getLados() <= 999);
    }

    // d) método que devolve o número de lados de um polígono regular através do seu nome.
    public Integer getLadosPeloNome(String nome) {

        if (nome.isEmpty()) {
            return null;
        }
        //todos os poligonos que estao nesta arvore sao regulares
        AVL<Poligono> arvoreCompleta = getArvoreBalanceada();
        Iterable<Poligono> poligonos = arvoreCompleta.inOrder();

        for (Poligono p : poligonos) {

            if (p.getNome().equals(nome)) {

                return p.getLados();
            }
        }
        return null;
    }

    // e) Dado um intervalo de num de lados, um método deve devolver os correspondentes nomes dos polígonos por ordem inversa, do maior para o menor número de lados.
    public String[] getNomeOrdemInversaPeloIntervalo(int valor1, int valor2) {

        ArrayList<String> aux = new ArrayList<>();

        // valida os valores do intervalo
        if (checkValores(valor1, valor2)) {
            return null;
        }

        if (valor1 > valor2) {
            int copy = valor1;
            valor1 = valor2;
            valor2 = copy;
        }

        // adiciona os nomes dos polígonos numa string invertendo a sua ordem pelo seu número de lados
        for (int i = valor2; i >= valor1; i--) {
            String pString = getNomePelosLados(i);
            if (pString != null) {
                aux.add(pString);
            }
        }

        // retorna os polígonos na ordem pretendida
        String[] nomesString = new String[aux.size()];
        for (int i = 0; i < aux.size(); i++) {
            nomesString[i] = aux.get(i);
        }
        return nomesString;
    }

    public boolean checkValores(int i, int j) {
        if (i > 999) {
            if (j > 999) {
                return true;
            }
            if (j < 0) {
                return true;
            }
        }
        if (j > 999) {
            if (i < 0) {
                return true;
            }
        }
        if (j < 0) {
            if (i < 0) {
                return true;
            }
        }
        return false;
    }

    // f) Dados dois nomes de polígonos encontrar o seu Lowest Common Ancestor (antecessor comum mais próximo) na árvore binária.
    public Poligono getLowestCommonAncestor(String nome1, String nome2) {

        AVL<Poligono> arvoreCompleta = getArvoreBalanceada();
        Iterable<Poligono> poligonos = arvoreCompleta.inOrder();
        Arvore nova = new Arvore(); //nova arvore com o metodo da alinea.

        if (nome1.isEmpty() || nome2.isEmpty()) {

            return null;
        }

        Integer lados1 = getLadosPeloNome(nome1);
        Integer lados2 = getLadosPeloNome(nome2);

        if (lados1 == null || lados2 == null) {

            return null;
        }
        // se os lados nao sao nulos, então, neste contexto, há poligonos com esses lados
        Poligono p1 = getPoligonoPelosLados(lados1);
        Poligono p2 = getPoligonoPelosLados(lados2);

        for (Poligono p : poligonos) {

            nova.insert(p); //insere na nova arvore todos os poligonos
        }

        return nova.getLowestCommonAncestor(p1, p2);
    }
}
