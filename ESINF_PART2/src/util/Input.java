/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import modelo.Local;
import modelo.Personagem;
import modelo.RedeAliancas;
import modelo.RedeEstradas;

/**
 *
 * @author user
 */
public class Input {

    public static ArrayList<String> lerFicheiroTodo(String nomeFich) throws FileNotFoundException {

        ArrayList<String> ficheiro = new ArrayList<>();

        Scanner s = new Scanner(new File(nomeFich));
        while (s.hasNext()) {
            ficheiro.add(s.nextLine());
        }
        return ficheiro;
    }

    public static void lerRedeEstradas(String nomeFich, RedeEstradas redeEstradas) throws FileNotFoundException {

        ArrayList<String> ficheiro = lerFicheiroTodo(nomeFich);

        for (String s : ficheiro) {
            List<String> dados = new ArrayList<>(Arrays.asList(s.split(",")));
            if (dados.size() == 2) {
                lerLocais(dados, redeEstradas);
            } else if (dados.size() == 3) {
                lerEstradas(dados, redeEstradas);
            }
        }
    }

    public static void lerLocais(List<String> dados, RedeEstradas redeEstradas) {

        String nome = dados.get(0);
        int dificuldade = Integer.parseInt(dados.get(1));

        Local novo = new Local(nome, dificuldade);
        redeEstradas.adicionarLocal(novo);
    }

    public static void lerEstradas(List<String> dados, RedeEstradas redeEstradas) {

        String nomeLocal = dados.get(0);
        String nomeOutroLocal = dados.get(1);
        int dificuldade = Integer.parseInt(dados.get(2));

        Local l1 = redeEstradas.getLocalPeloNome(nomeLocal);
        Local outro = redeEstradas.getLocalPeloNome(nomeOutroLocal);
        redeEstradas.adicionarEstrada(l1, outro, dificuldade);
    }

    public static void lerRedeAliancas(String nomeFich, RedeAliancas redeAliancas, RedeEstradas redeEstradas) throws FileNotFoundException {

        ArrayList<String> ficheiro = lerFicheiroTodo(nomeFich);

        for (String s : ficheiro) {
            List<String> dados = new ArrayList<>(Arrays.asList(s.split(",")));
            if (dados.size() == 3) {
                lerPersonagens(dados, redeAliancas, redeEstradas);
            } else if (dados.size() == 4) {
                lerAliancas(dados, redeAliancas);
            }
        }
    }

    public static void lerPersonagens(List<String> dados, RedeAliancas redeAliancas, RedeEstradas redeEstradas) {

        String nome = dados.get(0);
        int forca = Integer.parseInt(dados.get(1));
        String inicio = dados.get(2);

        Local localDeInicio = redeEstradas.getLocalPeloNome(inicio);
        Personagem nova = new Personagem(nome, forca, localDeInicio);
        redeAliancas.adicionarPersonagem(nova);
    }

    public static void lerAliancas(List<String> dados, RedeAliancas redeAliancas) {

        String nomePersonagem = dados.get(0);
        String outraPersonagem = dados.get(1);
        String tipo = dados.get(2);
        Float fatorCompatibilidade = Float.parseFloat(dados.get(3));

        Boolean tipoBoolean = null;
        if (tipo.equals("TRUE")) {
            tipoBoolean = true;
        } else if (tipo.equals("FALSE")) {
            tipoBoolean = false;
        }

        Personagem p = redeAliancas.getPersonagemPeloNome(nomePersonagem);
        Personagem aliado = redeAliancas.getPersonagemPeloNome(outraPersonagem);
        redeAliancas.adicionarAlianca(tipoBoolean, fatorCompatibilidade, p, aliado);
    }
}
