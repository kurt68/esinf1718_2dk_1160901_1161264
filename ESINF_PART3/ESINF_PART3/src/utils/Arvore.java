/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

import modelo.Poligono;

/**
 *
 * @author user
 */
public class Arvore extends AVL<Poligono> {

    // f) Dados dois nomes de polígonos encontrar o seu Lowest Common Ancestor (antecessor comum mais próximo) na árvore binária.
    public Poligono getLowestCommonAncestor(Poligono p1, Poligono p2) {

        return getLCA(root, p1, p2);
    }

    public Poligono getLCA(Node<Poligono> root, Poligono p1, Poligono p2) {

        if (root == null) {
            return null;
        }

        // se os ldaos dos poligonos sao mais pequenos que os lados da raiz, LCA cai à esq.
        if (root.getElement().getLados() > p1.getLados() && root.getElement().getLados() > p2.getLados()) {
            return getLCA(root.getLeft(), p1, p2);
        }

        // se forem maiores, LCA cai à direita.
        if (root.getElement().getLados() < p1.getLados() && root.getElement().getLados() < p2.getLados()) {
            return getLCA(root.getRight(), p1, p2);
        }

        return root.getElement();
    }

}
