/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.util.List;

/**
 *
 * @author Luís Santos
 */
public class ControloDoJogo {

    private RedeAliancas redeAliancas;
    private RedeEstradas redeEstradas;

    public ControloDoJogo(RedeAliancas redeAliancas, RedeEstradas redeEstradas) {
        this.redeAliancas = redeAliancas;
        this.redeEstradas = redeEstradas;

    }

    public void personagemPodeConquistarLocalJuntoComAliados(Personagem p, Local l) {

        System.out.println("Personagem: " + p + "\nLocal: " + l);
        Boolean consegueConquistar = false;
        Personagem aliadoConquistador = null;
        Integer dificuldade = null;
        List<Local> listaMinimaLocaisIntermedios = null;
        List<Personagem> aliados = redeAliancas.getAliadosAUmaPersonagem(p);

        for (Personagem pAliado : aliados) {

            List<Local> locaisIntermedios = redeEstradas.caminhoMaisCurtoEntrePersonagemELocalDesejado(l, p);
            Float forcaPersonagens = redeAliancas.forcaPersonagens(p, pAliado);
            dificuldade = redeEstradas.getForcaNecessariaParaConquistarLocal(l, p, redeAliancas);

            if (forcaPersonagens > dificuldade) {

                for (Local lIntermedio : locaisIntermedios) {

                    if (!aliados.contains(lIntermedio.getP())) {

                        consegueConquistar = true;
                        aliadoConquistador = pAliado;
                        listaMinimaLocaisIntermedios = locaisIntermedios;

                    }

                }

            }

        }

        if (consegueConquistar) {

            System.out.println("\nConsegue conquistar: " + consegueConquistar + "\nAliado: " + aliadoConquistador + "\nForca das Aliancas: " + redeAliancas.forcaPersonagens(p, aliadoConquistador) + "\nForca necessaria" + dificuldade);

            if (listaMinimaLocaisIntermedios != null) {

                System.out.println("\nLocaisIntermedios: ");

                for (Local lIntermedio : listaMinimaLocaisIntermedios) {

                    System.out.println(lIntermedio);

                }
            }
        } else {
            System.out.println("\nConsegue conquistar: " + consegueConquistar);
        }
    }
}
