/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import modelo.Poligono;

/**
 *
 * @author user
 */
public class Input {

    //ler oficheiro, separa por linhas e guarda os dados para criar poligono
    //para inserir na arvore
    public static AVL<Poligono> lerFicheiro(String ficheiro) throws FileNotFoundException {

        AVL<Poligono> arvore = new AVL<>();
        Scanner input = new Scanner(new File(ficheiro));

        while (input.hasNext()) {

            String linha = input.nextLine();
            String[] dados = linha.trim().split(";");

            int lados = Integer.parseInt(dados[0]);
            String nome = dados[1];
            Poligono p = new Poligono(lados, nome);

            arvore.insert(p);
        }
        return arvore;
    }

}
