/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo.registos;

import java.io.FileNotFoundException;
import java.util.Arrays;
import modelo.Poligono;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import utils.AVL;
import utils.Input;

/**
 *
 * @author user
 */
public class RegistoPoligonosTest {

    public RegistoPoligonosTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of getNomePelosLados method, of class RegistoPoligonos.
     *
     * @throws java.io.FileNotFoundException
     */
    @Test
    public void testGetNomePelosLados() throws FileNotFoundException { //alinea A e B done, leitura de Ficheiro esta correta!
        System.out.println("getNomePelosLados");
        Integer lados = 524;
        String expResult = "pentahectaicositetragon";
        RegistoPoligonos instance = new RegistoPoligonos();
        String result = instance.getNomePelosLados(lados);
        assertEquals(expResult, result);
    }

    @Test
    public void testGetArvoreBalanceada() throws FileNotFoundException { //arvore Balanceada, DONE!
        System.out.println("getArvoreBalanceada");
        AVL<Poligono> expected = Input.lerFicheiro("teste_lados_nome.txt");
        RegistoPoligonos instance = new RegistoPoligonos();
        AVL<Poligono> result = instance.getArvoreBalanceada();
        assertEquals(expected, result);
    }

    @Test
    public void testGetLadosPeloNome() throws FileNotFoundException { //alinea D done!
        System.out.println("getLadosPeloNome");
        Integer expected = 21;
        String nome = "icosihenagon";
        RegistoPoligonos instance = new RegistoPoligonos();
        Integer result = instance.getLadosPeloNome(nome);
        assertEquals(expected, result);
    }

    @Test
    public void testGetNomeOrdemInversaPeloIntervalo() throws FileNotFoundException { // alínea E done!
        System.out.println("getNomeOrdemInversaPeloIntervalo");
        int valor1 = 15;
        int valor2 = 20;
        String[] expected = {"icosagon", "enneakaidecagon", "octakaidecagon", "heptakaidecagon", "hexakaidecagon", "pentakaidecagon"};
        RegistoPoligonos instance = new RegistoPoligonos();
        String[] result = instance.getNomeOrdemInversaPeloIntervalo(valor1, valor2);
        assertTrue(Arrays.equals(expected, result));
    }

    @Test
    public void testGetLowestCommonAncestor() throws FileNotFoundException { //alinea F done!
        System.out.println("getLowestCommonAncestor");
        String nome1= "enneakaidecagon";//19 lados
        String nome2 = "icosihenagon";// 21 lados
        RegistoPoligonos instance = new RegistoPoligonos();
        Poligono result = instance.getLowestCommonAncestor(nome1, nome2);
        Poligono expected = new Poligono(20, "icosagon"); //o LCA é o pol. com 20 lados
        assertEquals(expected,result);
    }
}
