package registos;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import javafx.scene.Node;
import model.Cidadao;
import model.Reparticao;
import model.Senha;
import util.DoublyLinkedList;

/**
 *
 * @author user
 */
public class RegistoReparticoes {

    private DoublyLinkedList<Reparticao> reparticoes;
    private Map<Cidadao, Set<Senha>> map;

    public RegistoReparticoes(DoublyLinkedList<Reparticao> reparticoes, Map<Cidadao, Set<Senha>> map) {
        this.reparticoes = reparticoes;
        this.map = map;
    }

    public RegistoReparticoes() {
        reparticoes = new DoublyLinkedList<Reparticao>();
        map = new HashMap<Cidadao, Set<Senha>>();
    }

    public DoublyLinkedList<Reparticao> getReparticoes() {
        return reparticoes;
    }

    public Map<Cidadao, Set<Senha>> getMap() {
        return map;
    }

    public void setReparticoes(DoublyLinkedList<Reparticao> reparticoes) {
        this.reparticoes = reparticoes;
    }

    public void setMap(Map<Cidadao, Set<Senha>> map) {
        this.map = map;
    }

    
    public boolean addReparticao(Reparticao reparticao) {
        Iterator i = this.reparticoes.iterator();
        
        while(i.hasNext()) {
            
            Reparticao r = (Reparticao) i.next();
            if (r.getCodPostal().equals(reparticao.getCodPostal()) ) {
               return false;
            }
        }
        this.reparticoes.addFirst(reparticao);
        return true;
    }
    
    public boolean removeReparticao(Reparticao reparticao){ 
         Iterator i = this.reparticoes.iterator();
         boolean check=false;
         
         while(i.hasNext()) {
             Reparticao r = (Reparticao) i.next();
             if(r.equals(reparticao)) {
                 i.remove();
                 check= true;
             }
         }
         return check;
    }

    //alinea D)
    public Map<String, Set<Integer>> getCidadaosReparticao() {
        Map<String, Set<Integer>> cidadaosDaReparticao = new HashMap<>();
        Set<Integer> cidadaos = new HashSet<>();
        
        Iterator i = this.reparticoes.iterator();
        while (i.hasNext()) {
            Reparticao r = (Reparticao) i.next();
            String s = "cidade: "+ r.getCidade() + " ,numero de reparticao: " + r.getNum();
     
            for (Cidadao c : r.getListaCidadaos()) {
                String[] cod = c.getCodPostal().split("-");
                if(cod[0].equals(r.getCodPostal())) {
                    cidadaos.add(c.getNif());
                }
            }
            cidadaosDaReparticao.put(s, cidadaos);     
        }
        return cidadaosDaReparticao;
    }
    

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 71 * hash + Objects.hashCode(this.reparticoes);
        hash = 71 * hash + Objects.hashCode(this.map);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final RegistoReparticoes other = (RegistoReparticoes) obj;
        if (!Objects.equals(this.reparticoes, other.reparticoes)) {
            return false;
        }
        if (!Objects.equals(this.map, other.map)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "RegistoReparticoes{" + "reparticoes=" + reparticoes + ", map=" + map + '}';
    }
    
    

    
}
          
         
        
     
        



