/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util_tests;

import java.util.HashMap;
import java.util.HashSet;
import java.util.*;
import model.Cidadao;
import model.Reparticao;
import model.*;
import model.Servico;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import registos.RegistoCidadaos;
import registos.RegistoReparticoes;
import util.DoublyLinkedList;
import util.Input;

/**
 *
 * @author user
 */
public class InputTest {
    
    public InputTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testLeituraReparticao() throws Exception {
        System.out.println("Ler Reparticoes");
        RegistoReparticoes registoReparticaoA = new RegistoReparticoes();
        String file = "fx_reparticoes.txt";
        DoublyLinkedList<Servico> serv = new DoublyLinkedList();
        serv.addLast(new Servico('A'));
        serv.addLast(new Servico('B'));
        serv.addLast(new Servico('C'));
        
        Input instance = new Input();
        
        Reparticao r1 = new Reparticao("Vila das Aves", "4795", 1234, serv);
        Reparticao r2 = new Reparticao("Porto", "4555", 1235, serv);
        
        registoReparticaoA.getReparticoes().addFirst(r1);
        registoReparticaoA.getReparticoes().addFirst(r2);
 
        
        boolean aux = instance.leituraReparticao(file, registoReparticaoA);
        Assert.assertEquals(true, aux);
    }    
    
    @Test
    public void testLeituraSenha() throws Exception {
        System.out.println("Ler Senhas");
        String file = "fx_senhasRui.txt";
        String file2 = "fx_Rui.txt";
        Input instance = new Input();
        DoublyLinkedList<Servico> serv = new DoublyLinkedList();
        serv.addLast(new Servico('A'));
        serv.addLast(new Servico('B'));
        serv.addLast(new Servico('C'));
        
        Cidadao c1 = new Cidadao(111222333,"Rui","rui@gmail.com","4795-003",1234);
        
        Senha s1 = new Senha(1, 'A');
        Senha s2 = new Senha(1, 'C');

        Set<Senha> senhas = new HashSet<>();
        senhas.add(s1);
        senhas.add(s2);

        Map<Cidadao, Set<Senha>> teste = new HashMap<>();
        teste.put(c1, senhas);
        Map<Cidadao, Set<Senha>> coiso = instance.leituraSenhas(file,file2);
        Assert.assertTrue("Mesmo resultado", teste.equals(instance.leituraSenhas(file,file2)));
        
    }
    
    @Test
    public void testLeituraCidadaos() throws Exception {
        System.out.println("Ler Cidadaos");
        
        String file = "fx_cidadaos.txt";   
        Set<Cidadao> cidadaos = new HashSet<>();
        Cidadao c1 = new Cidadao(111222333,"Rui","rui@gmail.com","4795-003",1234);
        Cidadao c2 = new Cidadao(222333444,"Andre","andre@gmail.com","4555-999",1034);
        Cidadao c3 = new Cidadao(333444555,"Ze","ze@gmail.com","4795-002",1234);
        cidadaos.add(c1);
        cidadaos.add(c2);
        cidadaos.add(c3);
        Input instance = new Input();
        
        Assert.assertEquals(cidadaos, instance.leituraCidadao(file));
        
        
    }

}
