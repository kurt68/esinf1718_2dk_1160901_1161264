package model;

import registos.RegistoCidadaos;
import registos.RegistoReparticoes;

/**
 *
 * @author user
 */
public class Financas {
    
    private RegistoReparticoes registoReparticoes;
    private RegistoCidadaos registoCidadaos;
    
    private static final RegistoReparticoes REGISTO_REPARTICOES_OMISSAO = new RegistoReparticoes();
    private static final RegistoCidadaos REGISTO_CIDADAOS_OMISSAO = new RegistoCidadaos();
    
    public Financas() {
        registoReparticoes = REGISTO_REPARTICOES_OMISSAO;
        registoCidadaos = REGISTO_CIDADAOS_OMISSAO;
    }
    
    public Financas(RegistoReparticoes registoReparticoes, RegistoCidadaos registoCidadaos ) {
        this.registoReparticoes = registoReparticoes;
        this.registoCidadaos = registoCidadaos;
    }

    public RegistoReparticoes getRegistoReparticoes() {
        return registoReparticoes;
    }

    public RegistoCidadaos getRegistoCidadaos() {
        return registoCidadaos;
    }

    public void setRegistoReparticoes(RegistoReparticoes registoReparticoes) {
        this.registoReparticoes = registoReparticoes;
    }

    public void setRegistoCidadaos(RegistoCidadaos registoCidadaos) {
        this.registoCidadaos = registoCidadaos;
    }
    
    public void addRep (Reparticao r) {
  
        if(this.registoReparticoes.addReparticao(r) ) {
            this.registoCidadaos.moverCidadaosReparticao(r);
   
        }
    }
    
    public void removeRep(Reparticao r) {
        if(this.registoReparticoes.removeReparticao(r)) {
            this.registoCidadaos.moverCidadaosParaReparticaoProxima(r);
        }
    }
    
    public void addCidadao(Cidadao c) {
        this.registoCidadaos.addCidadao(c);
            
    }
    
}
