package registos;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Objects;
import java.util.Set;
import model.Cidadao;
import model.Reparticao;

/**
 *
 * @author user
 */
public class RegistoCidadaos {
    
    private Set<Cidadao> cidadaos;
    private RegistoReparticoes registoReparticoes;
    
    private static final Set<Cidadao> CIDADAOS_OMISSAO = new HashSet<>();
    private static final RegistoReparticoes REGISTO_REPARTICOES_OMISSAO = new RegistoReparticoes ();
    
    
    public RegistoCidadaos () {
        cidadaos = CIDADAOS_OMISSAO;
        registoReparticoes = REGISTO_REPARTICOES_OMISSAO;
    }
    
    public RegistoCidadaos (Set<Cidadao> cidadaos, RegistoReparticoes registoReparticoes) {
        
        this.cidadaos = cidadaos;
        this.registoReparticoes = registoReparticoes;
    }

    public Set<Cidadao> getCidadaos() {
        return cidadaos;
    }

    public RegistoReparticoes getRegistoReparticoes() {
        return registoReparticoes;
    }

    public void setCidadaos(Set<Cidadao> cidadaos) {
        this.cidadaos = cidadaos;
    }

    public void setRegistoReparticoes(RegistoReparticoes registoReparticoes) {
        this.registoReparticoes = registoReparticoes;
    }
    
    public boolean addCidadao (Cidadao c) {
        for(Cidadao cidadao : cidadaos ) {
            if(cidadao.getNif() == c.getNif()) {
                return false;
            }
        }
        this.cidadaos.add(c);
        return true;
    }
    
    
    
    
    public void moverCidadaosReparticao(Reparticao r) {
        
        for(Cidadao c : cidadaos) {
            if(c.getCodPostal().substring(0, 4).equals(r.getCodPostal().substring(0,4))) {
                
                c.setNumReparticao(r.getNum());
            }
        }
    }
    
    public void moverCidadaosParaReparticaoProxima (Reparticao r) {
        
        Reparticao reparticao = new Reparticao();
        int aux = 9999; 
                
        for(Cidadao c : cidadaos) {
            if(c.getNumReparticao() == r.getNum()) {
                Iterator i = this.registoReparticoes.getReparticoes().iterator();
                
                while(i.hasNext()) {
                
                    Reparticao rep = (Reparticao) i.next();
                    int cp = (Integer.parseInt(r.getCodPostal().substring(0, 4))) - (Integer.parseInt(rep.getCodPostal().substring(0, 4)));
                    cp = Math.abs(cp);
                    if(cp < aux && cp !=0) {
                        aux = cp;
                        reparticao = rep;
                    }
                }
            }
            c.setNumReparticao(reparticao.getNum());
     
        }
    }
    

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 43 * hash + Objects.hashCode(this.cidadaos);
        hash = 43 * hash + Objects.hashCode(this.registoReparticoes);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final RegistoCidadaos other = (RegistoCidadaos) obj;
        if (!Objects.equals(this.cidadaos, other.cidadaos)) {
            return false;
        }
        if (!Objects.equals(this.registoReparticoes, other.registoReparticoes)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "RegistoCidadaos{" + "cidadaos=" + cidadaos + ", registoReparticoes=" + registoReparticoes + '}';
    }
}
