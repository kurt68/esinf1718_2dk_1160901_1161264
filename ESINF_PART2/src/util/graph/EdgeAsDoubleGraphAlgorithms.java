package util.graph;

import java.util.LinkedList;
import static java.util.Collections.reverse;

/**
 *
 * @author DEI-ESINF
 */
public class EdgeAsDoubleGraphAlgorithms {

    /**
     * Determine the shortest path to all vertices from a vertex using
     * Dijkstra's algorithm To be called by public short method
     *
     * @param graph Graph object
     * @param sourceIdx Source vertex
     * @param knownVertices previously discovered vertices
     * @param verticesIndex index of vertices in the minimum path
     * @param minDist minimum distances in the path
     *
     */
    private static <V> void shortestPath(AdjacencyMatrixGraph<V, Integer> graph, int sourceIndex, boolean[] knownVertices, int[] verticesIndex, int[] minDist) {
       minDist[sourceIndex] = 0;
        while (sourceIndex != -1) {
            knownVertices[sourceIndex] = true;
            for (int i = 0; i < graph.numVertices; i++) {
                if (graph.privateGet(sourceIndex, i) != null) {
                    if (!knownVertices[i] && minDist[i] > (minDist[sourceIndex] + graph.privateGet(sourceIndex, i))) {
                        minDist[i] = minDist[sourceIndex] + graph.privateGet(sourceIndex, i);
                        verticesIndex[i] = sourceIndex;
                    }
                }
            }
            int min = Integer.MAX_VALUE;
            sourceIndex = -1;
            for (int i = 0; i < graph.numVertices; i++) {
                if (!knownVertices[i] && minDist[i] < min) {
                    min = minDist[i];
                    sourceIndex = i;
                }
            }
        }
    }

    

    /**
     * Determine the shortest path between two vertices using Dijkstra's
     * algorithm
     *
     * @param graph Graph object
     * @param source Source vertex
     * @param dest Destination vertices
     * @param path Returns the vertices in the path (empty if no path)
     * @return minimum distance, -1 if vertices not in graph or no path
     *
     */
    public static <V> int shortestPath(AdjacencyMatrixGraph<V, Integer> graph, V source, V dest, LinkedList<V> path) {
        int distance = 0;
        if (!graph.checkVertex(dest) || !graph.checkVertex(source)) {
            return -1;
        }
        boolean[] knownVertices = new boolean[graph.numVertices];
        int[] verticesIndex = new int[graph.numVertices];
        int[] minDist = new int[graph.numVertices];
        shortestPath(graph, graph.toIndex(source), knownVertices, verticesIndex, minDist);
        if (source == dest) {
            path.push(dest);
            return distance;
        }
        if (verticesIndex[graph.toIndex(dest)] == -1) {
            return -1;
        }
        path.clear();
        distance = distance + minDist[graph.toIndex(dest)];
        recreatePath(graph, graph.toIndex(source), graph.toIndex(dest), verticesIndex, path);
        reverse(path);
        return distance;
    }

 /**
     * Recreates the minimum path between two vertex, from the result of
     * Dikstra's algorithm
     *
     * @param graph Graph object
     * @param sourceIdx Source vertex
     * @param destIdx Destination vertices
     * @param verticesIndex index of vertices in the minimum path
     * @param Queue Vertices in the path (empty if no path)
     */
    private static <V> void recreatePath(AdjacencyMatrixGraph<V, Integer> graph, int sourceIdx, int destIdx, int[] verticesIndex, LinkedList<V> path) {

        path.add(graph.vertices.get(destIdx));
        if (sourceIdx != destIdx) {
            destIdx = verticesIndex[destIdx];
            recreatePath(graph, sourceIdx, destIdx, verticesIndex, path);
        }
    }

    /**
     * Creates new graph with minimum distances between all pairs uses the
     * Floyd-Warshall algorithm
     *
     * @param graph Graph object
     * @return the new graph
     */
    public static <V> AdjacencyMatrixGraph<V, Double> minDistGraph(AdjacencyMatrixGraph<V, Double> graph) {
        AdjacencyMatrixGraph<V, Double> newGraph = (AdjacencyMatrixGraph<V, Double>) graph.clone();

        for (int k = 0; k < newGraph.numVertices; k++) {
            for (int i = 0; i < newGraph.numVertices; i++) {
                if (i != k && newGraph.privateGet(i, k) != null) {
                    for (int j = 0; j < newGraph.numVertices; j++) {
                        if (i != j && j != k && newGraph.privateGet(k, j) != null) {
                            if (newGraph.privateGet(i, j) == null) {
                                newGraph.insertEdge(i, j, newGraph.privateGet(i, k) + newGraph.privateGet(k, j));
                            } else if (newGraph.privateGet(i, j) > newGraph.privateGet(i, k) + newGraph.privateGet(k, j)) {
                                newGraph.privateSet(i, j, newGraph.privateGet(i, k) + newGraph.privateGet(k, j));
                            }
                        }
                    }
                }
            }
        }
        return newGraph;
    }

}
