/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import util.graph.AdjacencyMatrixGraph;
import util.graph.EdgeAsDoubleGraphAlgorithms;
import util.graph.GraphAlgorithms;

/**
 *
 * @author user
 */
public class RedeEstradas implements Cloneable {

    private AdjacencyMatrixGraph<Local, Estrada> redeEstradas;

    public RedeEstradas() {
        redeEstradas = new AdjacencyMatrixGraph<>();
    }

    public int numLocais() {
        return redeEstradas.numVertices();
    }

    public int numEstradas() {
        return redeEstradas.numEdges();
    }

    public boolean adicionarLocal(Local l) {
        return (redeEstradas.insertVertex(l));
    }

    public boolean adicionarEstrada(Local l1, Local l2, int dificuldade) {
        return (redeEstradas.insertEdge(l1, l2, new Estrada(dificuldade)));
    }

    public Iterable<Estrada> estradasDeUmLocal(Local l) {
        return redeEstradas.outgoingEdges(l);
    }

    public Iterable<Local> locaisDiretamenteConectados(Local l) {
        return redeEstradas.directConnections(l);
    }

    public LinkedList<Local> verificarConexao(Local l1, Local l2) {

        if (!redeEstradas.checkVertex(l1) || !redeEstradas.checkVertex(l2)) {
            return null;
        }

        Local atual = l1; //o local de partida é o primeiro a verificar
        Set<Local> setLocaisConectados = new HashSet<>(); //guarda locais ligados ao atual
        LinkedList<Local> conexao = new LinkedList<>();
        conexao.add(l1);

        do {
            setLocaisConectados = (Set) this.locaisDiretamenteConectados(atual);
            atual = (Local) setLocaisConectados.iterator().next();
            if (setLocaisConectados.contains(l2)) {
                atual = l2;
            }
            while (conexao.contains(atual)) {
                atual = setLocaisConectados.iterator().next();
            }
            conexao.add(atual);
        } while (!(atual.equals(l2)));
        return conexao;
    }

    public Iterable<Local> irPeloNumeroMinimoDeEstradas(Local l1, Local l2) {

        if (!redeEstradas.checkVertex(l1) || !redeEstradas.checkVertex(l2)) {
            return null;
        }

        AdjacencyMatrixGraph<Local, Integer> grafo = cloneDificuldade();
        LinkedList<LinkedList<Local>> caminhos = new LinkedList<>();
        GraphAlgorithms.allPaths(grafo, l1, l2, caminhos);
        LinkedList<Local> returnList = new LinkedList<>();
        int i = 0;

        for (LinkedList<Local> l : caminhos) {
            if ((caminhos.indexOf(l) == 0) || EdgeAsDoubleGraphAlgorithms.shortestPath(grafo, l1, l2, l) < i) {
                i = EdgeAsDoubleGraphAlgorithms.shortestPath(grafo, l1, l2, l);
                returnList = l;
            }
        }
        return returnList;
    }

    public AdjacencyMatrixGraph<Local, Integer> cloneDificuldade() {

        AdjacencyMatrixGraph<Local, Integer> grafo = new AdjacencyMatrixGraph<>();

        for (Estrada e : redeEstradas.edges()) {
            Object[] l = redeEstradas.endVertices(e);
            grafo.insertEdge((Local) l[0], (Local) l[1], e.getDificuldade());
        }

        return grafo;
    }

    public Local getLocalPeloNome(String nome) {

        Iterator<Local> locaisAProcurar = redeEstradas.vertices().iterator();
        Local l = null;

        while (locaisAProcurar.hasNext()) {

            Local it = locaisAProcurar.next();
            String nomeIt = it.getNome();
            if (nomeIt.equals(nome)) {
                l = it;
            }
        }
        return l;
    }

    public double dificuldadeMinima(Local l1, Local l2, LinkedList<Local> caminho) {
        return EdgeAsDoubleGraphAlgorithms.shortestPath(cloneDificuldade(), l1, l2, caminho);
    }

    //adiciona um local a uma personagem, se a alianca da personagem consiguir conquistar o local
    public boolean adicionarLocalAPersonagemAlianca(Local l, Personagem p, Personagem outra, RedeAliancas redeAliancas) {
        //validacao

        if (!redeEstradas.checkVertex(l) || p.getLocaisDaPersonagem().contains(l) || redeAliancas.keyPersonagem(p) > 0 || redeAliancas.keyPersonagem(outra) > 0) {
            return false;
        }

        Float forcaAliados = redeAliancas.forcaPersonagens(p, outra);

        if (forcaAliados > getForcaNecessariaParaConquistarLocal(l, p, redeAliancas)) {
            adicionarLocalAPersonagem(l, p);
            return true;
        }

        return false;
    }
    
    public void adicionarLocalAPersonagem(Local l, Personagem p) {

        if (l.getP() != null) {
            l.getP().removerLocal(l);
        }
        l.setP(p);
        p.adicionarLocal(l);
    }

    public int getForcaNecessariaParaConquistarLocal(Local l, Personagem p, RedeAliancas redeAliancas) {

        int forca = 0;
        List<Local> caminhoMaisCurto = caminhoMaisCurtoEntrePersonagemELocalDesejado(l, p);

        for (Local i : caminhoMaisCurto) {
            if (!p.getLocaisDaPersonagem().contains(i) && !redeAliancas.localPertenceAliados(i, p)) {
                Personagem dono = i.getP();
                int forcaPersI = 0;
                if (dono != null) {
                    forcaPersI = dono.getForca();
                }
                int dificuldadeLocal = i.getDificuldade();
                forca += (forcaPersI + dificuldadeLocal);
            }
        }

        for (int i = 0; i < caminhoMaisCurto.size(); i++) {

            Local l1 = caminhoMaisCurto.get(i);
            Local l2 = caminhoMaisCurto.get(i + 1);
            Estrada e = redeEstradas.getEdge(l1, l2);
            forca += e.getDificuldade();
        }

        return forca;
    }

    public List<Local> caminhoMaisCurtoEntrePersonagemELocalDesejado(Local l, Personagem p) {

        Iterator<Local> locaisDaPersonagem = p.getLocaisDaPersonagem().iterator();
        //local a conquistar desejado ate ao local pertencente a personagem mais proximo
        Iterator<Local> caminhoMaisCurto = irPeloNumeroMinimoDeEstradas(locaisDaPersonagem.next(), l).iterator();

        while (locaisDaPersonagem.hasNext()) {
            Iterator<Local> c = irPeloNumeroMinimoDeEstradas(locaisDaPersonagem.next(), l).iterator();
            int size = 0;
            while (c.hasNext()) {
                size++;
                c.next();
            }

            int caminhoMaisCurtoSize = 0;

            while (caminhoMaisCurto.hasNext()) {

                caminhoMaisCurtoSize++;
                caminhoMaisCurto.next();
            }

            if (size <= caminhoMaisCurtoSize) {
                caminhoMaisCurto = c;
            }
        }

        List<Local> lista = new LinkedList<>();

        while (caminhoMaisCurto.hasNext()) {
            lista.add(caminhoMaisCurto.next());
        }

        return lista;
    }

    public boolean defenderLocalAlianca(Local l, Personagem atacante, Personagem aliado, RedeAliancas redeAliancas) {

        if (!redeEstradas.checkVertex(l) || atacante.getLocaisDaPersonagem().contains(l) || redeAliancas.keyPersonagem(atacante) > 0 || redeAliancas.keyPersonagem(aliado) > 0) {
            return false;
        }

        Personagem dono = l.getP();
        Float forcaAlianca = redeAliancas.forcaPersonagens(l.getP(), aliado);

        if ((float) atacante.getForca() > podePersonagemConquistarLocal(l, atacante, forcaAlianca)) {
            adicionarLocalAPersonagem(l, atacante);
            dono.setForca(dono.getForca() / 2);
            aliado.setForca(aliado.getForca() / 2);
            return false;
        }

        return true;
    }

    public Float podePersonagemConquistarLocal(Local l, Personagem p, Float forca) {

        Iterator<Local> locaisConectados = locaisDiretamenteConectados(l).iterator();
        Float forcaTotalNecessaria = null;

        while (locaisConectados.hasNext()) {
            Local i = locaisConectados.next();
            if (p.getLocaisDaPersonagem().contains(i)) {
                Estrada e = redeEstradas.getEdge(l, i);

                int dificuldadeEstrada = e.getDificuldade();
                int dificuldadeLocal = l.getDificuldade();
                forcaTotalNecessaria = forca + dificuldadeEstrada + dificuldadeLocal;
            }
        }

        return forcaTotalNecessaria;
    }
    
    public boolean getPodePersonagemConquistarLocal(Local l, Personagem p, RedeAliancas redeAliancas) {
        return (p.getForca() >= getForcaNecessariaParaConquistarLocal(l, p, redeAliancas));
    }
    
    @SuppressWarnings("unchecked")
    public RedeEstradas clone() {
        RedeEstradas newMap = new RedeEstradas();
        newMap.redeEstradas = (AdjacencyMatrixGraph<Local, Estrada>) redeEstradas.clone();
        return newMap;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 59 * hash + Objects.hashCode(this.redeEstradas);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final RedeEstradas other = (RedeEstradas) obj;
        if (!Objects.equals(this.redeEstradas, other.redeEstradas)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "RedeEstradas{" + "redeEstradas=" + redeEstradas + '}';
    }

}
