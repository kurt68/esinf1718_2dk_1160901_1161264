/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model_testes;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import model.Cidadao;
import model.Reparticao;
import model.Senha;
import model.Servico;
import org.junit.Assert;
import org.junit.Test;
import util.DoublyLinkedList;

/**
 *
 * @author Luís Santos
 */
public class ReparticaoTest {
    
    @Test
    public void procuraServicosTest() {
        System.out.println("Procura os servicos");
        Map<Servico, Integer> teste = new HashMap<>();
        DoublyLinkedList<Servico> listaServicos = new DoublyLinkedList<Servico>();
        DoublyLinkedList<Cidadao> listaCidadaos = new DoublyLinkedList<Cidadao>();
        Servico a = new Servico('A');
        Servico b = new Servico('B');
        listaServicos.addLast(a);
        listaServicos.addLast(b);
        Cidadao c1 = new Cidadao(111222333,"Rui","rui@gmail.com","4795-003",1234);
        Cidadao c2 = new Cidadao(222333444,"Andre","andre@gmail.com","4555-999",1034);
        Senha s1 = new Senha(1, 'A');
        Senha s2 = new Senha(2, 'A');
        Senha s3 = new Senha(1, 'B');
        c1.getSenhas().addLast(s1);
        c1.getSenhas().addLast(s3);
        c2.getSenhas().addLast(s2);
        listaCidadaos.addLast(c1);
        listaCidadaos.addLast(c2);
        Reparticao r1 = new Reparticao("Vila das Aves", "4795", 1234, listaServicos,listaCidadaos);
        teste.put(a, 2);
        teste.put(b, 1);
        Map<Servico, Integer> ts = r1.procuraServicos();
        Assert.assertTrue("Mesmo resultado", teste.equals(r1.procuraServicos()));
    }
    
    @Test
    public void servicosMaisProcuradosTest() {
        System.out.println("Servicos mais procurados");
        ArrayList<Servico> servicosMaisProcurados = new ArrayList<>();
        DoublyLinkedList<Servico> listaServicos = new DoublyLinkedList<Servico>();
        DoublyLinkedList<Cidadao> listaCidadaos = new DoublyLinkedList<Cidadao>();
        Servico a = new Servico('A');
        Servico b = new Servico('B');
        listaServicos.addLast(a);
        listaServicos.addLast(b);
        Cidadao c1 = new Cidadao(111222333,"Rui","rui@gmail.com","4795-003",1234);
        Cidadao c2 = new Cidadao(222333444,"Andre","andre@gmail.com","4555-999",1034);
        Senha s1 = new Senha(1, 'A');
        Senha s2 = new Senha(2, 'A');
        Senha s3 = new Senha(1, 'B');
        c1.getSenhas().addLast(s1);
        c1.getSenhas().addLast(s3);
        c2.getSenhas().addLast(s2);
        listaCidadaos.addLast(c1);
        listaCidadaos.addLast(c2);
        Reparticao r1 = new Reparticao("Vila das Aves", "4795", 1234, listaServicos,listaCidadaos);
        servicosMaisProcurados.add(a);
        Assert.assertTrue("Mesmo resultado", servicosMaisProcurados.equals(r1.servicosMaisProcurados()));
    }

    @Test
    public void cidadaoAbandonaFilasTest() {
        System.out.println("Cidadao abandona as filas");
        DoublyLinkedList<Servico> listaServicos = new DoublyLinkedList<Servico>();
        DoublyLinkedList<Cidadao> listaCidadaos = new DoublyLinkedList<Cidadao>();
        Servico a = new Servico('A');
        Servico b = new Servico('B');
        listaServicos.addLast(a);
        listaServicos.addLast(b);
        Cidadao c1 = new Cidadao(111222333,"Rui","rui@gmail.com","4795-003",1234);
        Senha s1 = new Senha(1, 'A');
        Senha s3 = new Senha(1, 'B');
        c1.getSenhas().addLast(s1);
        c1.getSenhas().addLast(s3);
        listaCidadaos.addLast(c1);
        Reparticao r1 = new Reparticao("Vila das Aves", "4795", 1234, listaServicos,listaCidadaos);
        r1.cidadaoAbandonaFilas(c1);
        Assert.assertTrue("Tem que retornar true", c1.getSenhas().isEmpty()==true);
    }
}
