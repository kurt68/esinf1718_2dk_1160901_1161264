package model;

/**
 *
 * @author user
 */
public class Senha {
    
    // atributos
    private int numOrdem;
    private char codigo;
    
    
    //variaveis omissao
    private final int NUM_ORDEM_OMISSAO = 0;
    private final char CODIGO_OMISSAO = 'X';
    
    
    public Senha () {
        numOrdem = NUM_ORDEM_OMISSAO;
        codigo = CODIGO_OMISSAO;
    }
    
    public Senha (int numOrdem, char codigo) {
        this.numOrdem = numOrdem;
        this.codigo = codigo;
    }

    public int getNumOrdem() {
        return numOrdem;
    }

    public char getCodigo() {
        return codigo;
    }

    public void setNumOrdem(int numOrdem) {
        this.numOrdem = numOrdem;
    }

    public void setCodigo(char codigo) {
        this.codigo = codigo;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 17 * hash + this.numOrdem;
        hash = 17 * hash + this.codigo;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Senha other = (Senha) obj;
        if (this.numOrdem != other.numOrdem) {
            return false;
        }
        if (this.codigo != other.codigo) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Senha{" + "numOrdem=" + numOrdem + ", codigo=" + codigo + '}';
    }
    
    
}
