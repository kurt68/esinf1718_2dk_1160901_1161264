/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.util.ArrayList;
import java.util.Objects;

/**
 *
 * @author user
 */
public class Personagem {
    
    private String nome;
    private int forca;
    private ArrayList<Local> locaisDaPersonagem = new ArrayList<>();
    private Local inicial;
    
    private static final String NOME_OMISSAO = "String_Omissao";
    private static final int FORCA_OMISSAO = 0;
    
    public Personagem(String nome, int forca, Local inicial){
        this.nome=nome;
        this.forca=forca;
        this.inicial=inicial;
        adicionarLocal(inicial);
    }
    
    public Personagem () {
        nome=NOME_OMISSAO;
        forca=FORCA_OMISSAO;
    }

    public String getNome() {
        return nome;
    }

    public int getForca() {
        return forca;
    }

    public ArrayList<Local> getLocaisDaPersonagem() {
        return locaisDaPersonagem;
    }

    public Local getInicial() {
        return inicial;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public void setForca(int forca) {
        this.forca = forca;
    }

    public void setLocaisDaPersonagem(ArrayList<Local> locaisDaPersonagem) {
        this.locaisDaPersonagem = locaisDaPersonagem;
    }

    public void setInicial(Local inicial) {
        this.inicial = inicial;
    }
    
    public boolean adicionarLocal(Local l) {
        return locaisDaPersonagem.add(l);
    }
    
    public boolean removerLocal(Local l) {
        return locaisDaPersonagem.remove(l);
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 59 * hash + Objects.hashCode(this.nome);
        hash = 59 * hash + this.forca;
        hash = 59 * hash + Objects.hashCode(this.locaisDaPersonagem);
        hash = 59 * hash + Objects.hashCode(this.inicial);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Personagem other = (Personagem) obj;
        if (this.forca != other.forca) {
            return false;
        }
        if (!Objects.equals(this.nome, other.nome)) {
            return false;
        }
        if (!Objects.equals(this.locaisDaPersonagem, other.locaisDaPersonagem)) {
            return false;
        }
        if (!Objects.equals(this.inicial, other.inicial)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Personagem{" + "nome=" + nome + ", forca=" + forca + ", locaisDaPersonagem=" + locaisDaPersonagem + ", inicial=" + inicial + '}';
    }   
}
