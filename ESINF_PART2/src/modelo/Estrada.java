/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 *
 * @author user
 */
public class Estrada {
    
    private int dificuldade;
    
    private static final int DIFICULDADE_OMISSAO= 0;
    
    public Estrada(int dificuldade) {
        this.dificuldade=dificuldade;
    }
    
    public Estrada() {
        dificuldade=DIFICULDADE_OMISSAO;
    }

    public int getDificuldade() {
        return dificuldade;
    }

    public void setDificuldade(int dificuldade) {
        this.dificuldade = dificuldade;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 79 * hash + this.dificuldade;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Estrada other = (Estrada) obj;
        if (this.dificuldade != other.dificuldade) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Estrada{" + "dificuldade=" + dificuldade + '}';
    }
}
