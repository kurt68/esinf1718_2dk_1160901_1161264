/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.util.Objects;

/**
 *
 * @author user
 */
public class Local {
    
    private String nome;
    private Personagem p;
    private int dificuldade;
    
    private static final String NOME_OMISSAO= "String_Omissao";
    private static final Personagem P_OMISSAO= null; //por omissao o local fica sem personagem associada
    private static final int DIFICULDADE_OMISSAO = 0;
    
    public Local(String nome, Personagem p, int dificuldade) {
        this.nome=nome;
        this.p=p;
        this.dificuldade=dificuldade;
    }
    
    public Local() {
        nome=NOME_OMISSAO;
        p=P_OMISSAO;
        dificuldade=DIFICULDADE_OMISSAO;
    }
    
    public Local(String nome, int dificuldade) {
        this.nome=nome;
        this.dificuldade=dificuldade;
    }

    public String getNome() {
        return nome;
    }

    public Personagem getP() {
        return p;
    }

    public int getDificuldade() {
        return dificuldade;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public void setP(Personagem p) {
        this.p = p;
    }

    public void setDificuldade(int dificuldade) {
        this.dificuldade = dificuldade;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 37 * hash + Objects.hashCode(this.nome);
        hash = 37 * hash + Objects.hashCode(this.p);
        hash = 37 * hash + this.dificuldade;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Local other = (Local) obj;
        if (this.dificuldade != other.dificuldade) {
            return false;
        }
        if (!Objects.equals(this.nome, other.nome)) {
            return false;
        }
        if (!Objects.equals(this.p, other.p)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Local{" + "nome=" + nome + ", p=" + p + ", dificuldade=" + dificuldade + '}';
    }    
}
