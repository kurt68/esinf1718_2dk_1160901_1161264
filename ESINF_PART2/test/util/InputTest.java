/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import java.util.ArrayList;
import java.util.List;
import modelo.Local;
import modelo.Personagem;
import modelo.RedeAliancas;
import modelo.RedeEstradas;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author Luís Santos
 */
public class InputTest {

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }
    
    RedeEstradas redeEstradas = new RedeEstradas();
    RedeAliancas redeAliancas = new RedeAliancas();
    ArrayList<String> expResult = new ArrayList<>();
    
    @Before
    public void setUp() {
        
        expResult.add("LOCAIS");
        expResult.add("Local0,30");
        expResult.add("Local1,29");
        expResult.add("Local2,21");
        expResult.add("Local3,23");
        expResult.add("Local4,30");
        expResult.add("Local5,23");
        expResult.add("Local6,27");
        expResult.add("Local7,26");
        expResult.add("Local8,27");
        expResult.add("Local9,29");
        expResult.add("Local10,25");
        expResult.add("Local11,36");
        expResult.add("Local12,22");
        expResult.add("Local13,31");
        expResult.add("Local14,26");
        expResult.add("Local15,38");
        expResult.add("Local16,21");
        expResult.add("Local17,34");
        expResult.add("Local18,30");
        expResult.add("Local19,33");
        expResult.add("Local20,25");
        expResult.add("Local21,32");
        expResult.add("Local22,32");
        expResult.add("Local23,26");
        expResult.add("Local24,34");
        expResult.add("Local25,30");
        expResult.add("Local26,34");
        expResult.add("Local27,38");
        expResult.add("Local28,20");
        expResult.add("Local29,31");
        expResult.add("CAMINHOS");
        expResult.add("Local0,Local1,29");
        expResult.add("Local0,Local3,20");
        expResult.add("Local0,Local6,26");
        expResult.add("Local0,Local9,20");
        expResult.add("Local0,Local12,23");
        expResult.add("Local0,Local15,23");
        expResult.add("Local0,Local18,29");
        expResult.add("Local0,Local19,28");
        expResult.add("Local0,Local22,23");
        expResult.add("Local0,Local23,22");
        expResult.add("Local0,Local26,27");
        expResult.add("Local0,Local28,24");
        expResult.add("Local1,Local4,21");
        expResult.add("Local1,Local8,25");
        expResult.add("Local1,Local9,29");
        expResult.add("Local1,Local14,25");
        expResult.add("Local1,Local15,28");
        expResult.add("Local1,Local16,20");
        expResult.add("Local1,Local17,26");
        expResult.add("Local2,Local3,27");
        expResult.add("Local2,Local5,24");
        expResult.add("Local2,Local11,26");
        expResult.add("Local2,Local14,24");
        expResult.add("Local2,Local22,26");
        expResult.add("Local2,Local27,24");
        expResult.add("Local3,Local9,26");
        expResult.add("Local3,Local11,27");
        expResult.add("Local3,Local14,26");
        expResult.add("Local3,Local16,21");
        expResult.add("Local3,Local17,28");
        expResult.add("Local3,Local21,28");
        expResult.add("Local3,Local22,29");
        expResult.add("Local4,Local6,28");
        expResult.add("Local4,Local7,24");
        expResult.add("Local4,Local9,25");
        expResult.add("Local4,Local16,25");
        expResult.add("Local4,Local17,22");
        expResult.add("Local4,Local21,29");
        expResult.add("Local4,Local24,26");
        expResult.add("Local5,Local10,28");
        expResult.add("Local5,Local11,25");
        expResult.add("Local5,Local12,24");
        expResult.add("Local5,Local15,27");
        expResult.add("Local5,Local17,25");
        expResult.add("Local5,Local24,20");
        expResult.add("Local5,Local26,20");
        expResult.add("Local6,Local8,25");
        expResult.add("Local6,Local9,20");
        expResult.add("Local6,Local15,21");
        expResult.add("Local6,Local20,29");
        expResult.add("Local6,Local21,28");
        expResult.add("Local6,Local28,21");
        expResult.add("Local6,Local29,29");
        expResult.add("Local7,Local8,29");
        expResult.add("Local7,Local13,25");
        expResult.add("Local7,Local19,21");
        expResult.add("Local7,Local25,20");
        expResult.add("Local7,Local27,22");
        expResult.add("Local8,Local10,28");
        expResult.add("Local8,Local13,22");
        expResult.add("Local8,Local17,25");
        expResult.add("Local8,Local18,29");
        expResult.add("Local8,Local20,29");
        expResult.add("Local8,Local21,22");
        expResult.add("Local8,Local24,26");
        expResult.add("Local9,Local10,26");
        expResult.add("Local9,Local11,23");
        expResult.add("Local9,Local20,29");
        expResult.add("Local9,Local27,28");
        expResult.add("Local10,Local12,23");
        expResult.add("Local10,Local16,27");
        expResult.add("Local10,Local19,27");
        expResult.add("Local10,Local25,24");
        expResult.add("Local10,Local29,23");
        expResult.add("Local11,Local14,27");
        expResult.add("Local11,Local18,22");
        expResult.add("Local11,Local26,20");
        expResult.add("Local11,Local29,21");
        expResult.add("Local12,Local14,21");
        expResult.add("Local12,Local25,21");
        expResult.add("Local12,Local26,27");
        expResult.add("Local12,Local28,25");
        expResult.add("Local13,Local15,24");
        expResult.add("Local13,Local17,25");
        expResult.add("Local13,Local18,26");
        expResult.add("Local13,Local21,20");
        expResult.add("Local13,Local24,22");
        expResult.add("Local13,Local26,24");
        expResult.add("Local13,Local29,21");
        expResult.add("Local14,Local18,25");
        expResult.add("Local14,Local19,21");
        expResult.add("Local14,Local26,22");
        expResult.add("Local14,Local28,23");
        expResult.add("Local15,Local16,27");
        expResult.add("Local15,Local17,27");
        expResult.add("Local15,Local22,22");
        expResult.add("Local15,Local24,20");
        expResult.add("Local15,Local27,27");
        expResult.add("Local16,Local19,20");
        expResult.add("Local16,Local20,25");
        expResult.add("Local16,Local23,23");
        expResult.add("Local16,Local27,22");
        expResult.add("Local16,Local28,25");
        expResult.add("Local17,Local20,26");
        expResult.add("Local17,Local24,26");
        expResult.add("Local17,Local26,25");
        expResult.add("Local18,Local22,28");
        expResult.add("Local18,Local25,23");
        expResult.add("Local18,Local27,21");
        expResult.add("Local18,Local28,29");
        expResult.add("Local19,Local22,23");
        expResult.add("Local20,Local21,27");
        expResult.add("Local20,Local23,26");
        expResult.add("Local20,Local25,28");
        expResult.add("Local20,Local29,29");
        expResult.add("Local21,Local22,22");
        expResult.add("Local21,Local23,24");
        expResult.add("Local21,Local24,21");
        expResult.add("Local21,Local25,23");
        expResult.add("Local21,Local27,24");
        expResult.add("Local21,Local29,29");
        expResult.add("Local22,Local23,24");
        expResult.add("Local22,Local25,27");
        expResult.add("Local22,Local28,20");
        expResult.add("Local22,Local29,24");
        expResult.add("Local23,Local24,25");
        expResult.add("Local24,Local26,21");
        expResult.add("Local25,Local26,22");
        expResult.add("Local25,Local29,22");
        expResult.add("Local26,Local28,22");
        expResult.add("Local27,Local28,20");
        expResult.add("Local28,Local29,22");
        expResult.add("Local29,Local30,23");
        
    }
    
    @After 
    public void tearDown() {
        
    }
    /**
     * Test of lerFicheiroTodo method, of class Input.
     */
    @Test
    public void testLerFicheiroTodo() throws Exception {
        
        System.out.println("lerFicheiroTodo");
        String nomeFich = "locais_S.txt";
        ArrayList<String> result = Input.lerFicheiroTodo(nomeFich);
        assertEquals(expResult, result);
  
    }

    /**
     * Test of lerRedeEstradas method, of class Input.
     */
    @Test
    public void testLerRedeEstradas() throws Exception {
        
        System.out.println("lerRedeEstradas");
        String nomeFich = "locais_S.txt";
        RedeEstradas result = new RedeEstradas();
        Input.lerRedeEstradas(nomeFich, result);
        
        Local local0 = new Local("Local0",30);
        Local local1 = new Local("Local1",29);
        Local local2 = new Local("Local2",21);
        Local local3 = new Local("Local3",23);
        Local local4 = new Local("Local4",30);
        Local local5 = new Local("Local5",23);
        Local local6 = new Local("Local6",27);
        Local local7 = new Local("Local7",26);
        Local local8 = new Local("Local8",27);
        Local local9 = new Local("Local9",29);
                
        redeEstradas.adicionarLocal(local0);
        redeEstradas.adicionarLocal(local1);
        redeEstradas.adicionarLocal(local2);
        redeEstradas.adicionarLocal(local3);
        redeEstradas.adicionarLocal(local4);
        redeEstradas.adicionarLocal(local5);
        redeEstradas.adicionarLocal(local6);
        redeEstradas.adicionarLocal(local7);
        redeEstradas.adicionarLocal(local8);
        redeEstradas.adicionarLocal(local9);
        
        redeEstradas.adicionarEstrada(local0,local1,29);
        redeEstradas.adicionarEstrada(local0,local3,20);
        redeEstradas.adicionarEstrada(local0,local6,26);
        redeEstradas.adicionarEstrada(local0,local9,20);
        redeEstradas.adicionarEstrada(local1,local4,20);
        redeEstradas.adicionarEstrada(local1,local8,25);
        redeEstradas.adicionarEstrada(local1,local9,29);
        redeEstradas.adicionarEstrada(local2,local3,27);
        redeEstradas.adicionarEstrada(local2,local5,24);
        redeEstradas.adicionarEstrada(local3,local9,26);
        redeEstradas.adicionarEstrada(local4,local6,28);
        redeEstradas.adicionarEstrada(local4,local7,24);
        redeEstradas.adicionarEstrada(local4,local9,25);
        redeEstradas.adicionarEstrada(local6,local8,25);
        redeEstradas.adicionarEstrada(local6,local9,20);
        redeEstradas.adicionarEstrada(local7,local8,29);
        
        assertEquals(redeEstradas, result);

    }

    /**
     * Test of lerLocais method, of class Input.
     */
    @Test
    public void testLerLocais() {
        
        System.out.println("lerLocais");
        List<String> dados = new ArrayList<>();
        dados.add("Local0");
        dados.add("30");
        RedeEstradas redeEstradas = new RedeEstradas();
        Input.lerLocais(dados, redeEstradas);
        Local local0 = new Local("Local0", 30);
        redeEstradas.adicionarLocal(local0);
        Local localResult = redeEstradas.getLocalPeloNome("Local0");
        Local localExpected = redeEstradas.getLocalPeloNome("Local0");
        
        assertEquals(localExpected, localResult);
    }

    /**
     * Test of lerEstradas method, of class Input.
     */
    @Test
    public void testLerEstradas() {
        
        System.out.println("lerEstradas");
        List<String> dados = new ArrayList<>();
        dados.add("Local0");
        dados.add("Local1");
        dados.add("29");
        Local local0 = new Local("Local0", 30);
        redeEstradas.adicionarLocal(local0);
        Local local1 = new Local("Local1", 29);
        redeEstradas.adicionarLocal(local1);
        redeEstradas.adicionarEstrada(local0, local1, 29);
  
    } 

    /**
     * Test of lerRedeAlaincas method, of class Input.
     */
    @Test
    public void testLerRedeAliancas() throws Exception {
        
        System.out.println("lerRedeAliancas");
        String nomeFich = "pers_S.txt";
        RedeAliancas result = new RedeAliancas();
        Input.lerRedeAliancas(nomeFich, redeAliancas, redeEstradas);
        
        Local local10 = new Local("Local10",25);
        Local local12 = new Local("Local12",22);
        Local local14 = new Local("Local14",26);
        Local local16 = new Local("Local16",21);
        Local local18 = new Local("Local18",30);
        Local local20 = new Local("Local20",25);
         
        Personagem pers0 = new Personagem ("Pers0",195,local12);
        Personagem pers1 = new Personagem ("Pers1",111,local14);
        Personagem pers2 = new Personagem ("Pers2",112,local16);
        Personagem pers3 = new Personagem ("Pers3",429,local18);
        Personagem pers4 = new Personagem ("Pers4",262,local10);
        Personagem pers5 = new Personagem ("Pers5",126,local12);
        Personagem pers6 = new Personagem ("Pers6",121,local14);
        Personagem pers7 = new Personagem ("Pers7",354,local16);
        Personagem pers8 = new Personagem ("Pers8",481,local16);
        Personagem pers9 = new Personagem ("Pers9",463,local20);
                
        redeAliancas.adicionarPersonagem(pers0);
        redeAliancas.adicionarPersonagem(pers1);
        redeAliancas.adicionarPersonagem(pers2);
        redeAliancas.adicionarPersonagem(pers3);
        redeAliancas.adicionarPersonagem(pers4);
        redeAliancas.adicionarPersonagem(pers5);
        redeAliancas.adicionarPersonagem(pers6);
        redeAliancas.adicionarPersonagem(pers7);
        redeAliancas.adicionarPersonagem(pers8);
        redeAliancas.adicionarPersonagem(pers9);
        
        redeAliancas.adicionarAlianca(true, Float.MAX_VALUE, pers0, pers3);
        redeAliancas.adicionarAlianca(true, Float.MAX_VALUE, pers0, pers4);
        redeAliancas.adicionarAlianca(true, Float.MAX_VALUE, pers1, pers2);
        redeAliancas.adicionarAlianca(true, Float.MAX_VALUE, pers1, pers3);
        redeAliancas.adicionarAlianca(true, Float.MAX_VALUE, pers1, pers4);
        redeAliancas.adicionarAlianca(true, Float.MAX_VALUE, pers3, pers4);
        redeAliancas.adicionarAlianca(false, Float.MAX_VALUE, pers5, pers7);
        redeAliancas.adicionarAlianca(true, Float.MAX_VALUE, pers6, pers8);
        redeAliancas.adicionarAlianca(true, Float.MAX_VALUE, pers7, pers8);
        redeAliancas.adicionarAlianca(true, Float.MAX_VALUE, pers7, pers9);
        
        assertEquals(redeAliancas, result);

    }

    /**
     * Test of lerPersonagens method, of class Input.
     */
    @Test
    public void testLerPersonagens() {
        
        System.out.println("lerPersonagens");
        List<String> dados = new ArrayList<>();
        dados.add("Pers0");
        dados.add("195");
        dados.add("Local2");
        Local local2 = new Local("Local2",21);
        Personagem pers0 = new Personagem("Pers0", 195, local2);
        redeAliancas.adicionarPersonagem(pers0);
        Personagem personagemResult = redeAliancas.getPersonagemPeloNome("Pers0");
        Personagem personagemExpected = redeAliancas.getPersonagemPeloNome("Pers0");
        
        assertEquals(personagemExpected, personagemResult);

    }

    
    /**
     * Test of lerAliancas method, of class Input.
     */
    @Test
    public void testLerAliancas() {
        
        System.out.println("lerAliancas");
        List<String> dados = new ArrayList<>();
        dados.add("Pers0");
        dados.add("Pers3");
        dados.add("TRUE");
        dados.add("0.8");
        Local local2 = new Local("Local2",21);
        Local local8 = new Local("Local8",27);
        Personagem pers0 = new Personagem ("Pers0", 195, local2);
        redeAliancas.adicionarPersonagem(pers0);
        Personagem pers3 = new Personagem ("Pers3", 429, local8);
        redeAliancas.adicionarPersonagem(pers3);
        redeAliancas.adicionarAlianca(true, Float.MAX_VALUE, pers0, pers3);

    }
}
