package model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import util.DoublyLinkedList;

/**
 *
 * @author user
 */
public class Reparticao {

    // atributos
    private String cidade;
    private String codPostal;
    private int num;
    private DoublyLinkedList<Servico> listaServicos;
    private DoublyLinkedList<Cidadao> listaCidadaos;

    // variaveis por omissao
    private final String CIDADE_OMISSAO = "Sem cidade";
    private final String COD_POSTAL_OMISSAO = "0000-000";
    private final int NUM_OMISSAO = 0000;

    public Reparticao() {
        cidade = CIDADE_OMISSAO;
        codPostal = COD_POSTAL_OMISSAO;
        num = NUM_OMISSAO;
        listaServicos = new DoublyLinkedList<Servico>();
        listaCidadaos = new DoublyLinkedList<Cidadao>();
    }

    public Reparticao(String cidade, String codPostal, int num, DoublyLinkedList<Servico> listaServicos) {

        this.cidade = cidade;
        this.codPostal = codPostal;
        this.num = num;
        this.listaServicos = listaServicos;
    }

    public Reparticao(String cidade, String codPostal, int num, DoublyLinkedList<Servico> listaServicos, DoublyLinkedList<Cidadao> listaCidadaos) {

        this.cidade = cidade;
        this.codPostal = codPostal;
        this.num = num;
        this.listaServicos = listaServicos;
        this.listaCidadaos = listaCidadaos;
    }

    // gets
    public String getCidade() {
        return cidade;
    }

    public String getCodPostal() {
        return codPostal;
    }

    public int getNum() {
        return num;
    }

    public DoublyLinkedList<Servico> getListaServicos() {
        return listaServicos;
    }

    public DoublyLinkedList<Cidadao> getListaCidadaos() {
        return listaCidadaos;
    }

    // sets
    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    public void setCodPostal(String codPostal) {
        this.codPostal = codPostal;
    }

    public void setNum(int num) {
        this.num = num;
    }

    public void setListaServicos(DoublyLinkedList<Servico> listaServicos) {
        this.listaServicos = listaServicos;
    }

    public void setListaCidadaos(DoublyLinkedList<Cidadao> listaCidadaos) {
        this.listaCidadaos = listaCidadaos;
    }

    //alínea G)
    public Map<Servico, Integer> procuraServicos() {
        Map<Servico, Integer> servicos = new HashMap<>();
        Iterator<Servico> it = listaServicos.iterator();

        while (it.hasNext()) {
            Servico servico = it.next();
            Integer size = 0;
            Iterator<Cidadao> i = listaCidadaos.iterator();
            while (i.hasNext()) {
                Cidadao cidadao = i.next();
                DoublyLinkedList<Senha> senhas = cidadao.getSenhas();
                Iterator<Senha> its = senhas.iterator();
                while (its.hasNext()) {
                    Senha senha = its.next();
                    if (senha.getCodigo() == servico.getCodigo()) {
                        size++;
                    }

                }
                servicos.put(servico, size);
            }

        }
        return servicos;

    }

    public ArrayList<Servico> servicosMaisProcurados() {
        Map<Servico, Integer> servicos = this.procuraServicos();
        ArrayList<Servico> servicosMaisProcurados = new ArrayList<>();
        ArrayList<Integer> sizes = new ArrayList<>();
        Set<Servico> sv = servicos.keySet();
        ArrayList<Servico> svs = new ArrayList<>();
        Iterator<Servico> t = sv.iterator();
        while (t.hasNext()) {
            Servico s = t.next();
            sizes.add(servicos.get(s));
            svs.add(s);
        }
        int max = sizes.get(0);
        for (int i = 0; i < svs.size(); i++) {
            if (sizes.get(i) > max) {
                max = sizes.get(i);
            }
        }
        for (int i = 0; i < svs.size(); i++) {
            if (sizes.get(i) == max) {
                servicosMaisProcurados.add(svs.get(i));
            }
        }
        return servicosMaisProcurados;
    }

    //alínea H)
    public void cidadaoAbandonaFilas(Cidadao cidadao) {

        Iterator<Cidadao> i = this.listaCidadaos.iterator();

        while (i.hasNext()) {
            Cidadao c = i.next();
            if (c.equals(cidadao)) {
                Iterator<Senha> it = c.getSenhas().iterator();
                while (it.hasNext()) {
                    it.next();
                    it.remove();
                }

            }
        }

    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 59 * hash + Objects.hashCode(this.cidade);
        hash = 59 * hash + Objects.hashCode(this.codPostal);
        hash = 59 * hash + this.num;
        hash = 59 * hash + Objects.hashCode(this.listaServicos);
        hash = 59 * hash + Objects.hashCode(this.listaCidadaos);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Reparticao other = (Reparticao) obj;
        if (this.num != other.num) {
            return false;
        }
        if (!Objects.equals(this.cidade, other.cidade)) {
            return false;
        }
        if (!Objects.equals(this.codPostal, other.codPostal)) {
            return false;
        }
        if (!Objects.equals(this.listaServicos, other.listaServicos)) {
            return false;
        }
        if (!Objects.equals(this.listaCidadaos, other.listaCidadaos)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Reparticao{" + "cidade=" + cidade + ", codPostal=" + codPostal + ", num=" + num + ", listaServicos=" + listaServicos + ", listaCidadaos=" + listaCidadaos + '}';
    }

}
