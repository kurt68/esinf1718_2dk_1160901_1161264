 /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.util.LinkedList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import util.graphbase.Graph;

/**
 *
 * @author user
 */
public class RedeAliancasTest {
    
    public RedeAliancasTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of adicionarPersonagem method, of class RedeAliancas.
     */
    @Test
    public void testAdicionarPersonagem() {
        
        System.out.println("adicionarPersonagem");
        Local local1 = new Local("local1", 20);
        Personagem personagem = new Personagem("p1", 70, local1);
        
        RedeAliancas instance = new RedeAliancas();
        instance.adicionarPersonagem(personagem);
        
        Personagem result = instance.getPersonagemPeloNome("p1");
        Personagem expResult = personagem;
        
        assertEquals(expResult, result);
    }

    /**
     * Test of adicionarAlianca method, of class RedeAliancas.
     */
    @Test
    public void testAdicionarAlianca() {
        
        System.out.println("adicionarAlianca");
        Personagem pers1 = new Personagem("p1", 60, new Local());
        Personagem pers2 = new Personagem("p2", 50, new Local());
        Float compatibilidade = 0.8f;
        boolean publica = true;
        
        RedeAliancas instance = new RedeAliancas();
        
        boolean expResult = true;
        boolean result = instance.adicionarAlianca(publica, compatibilidade, pers1, pers2);
        
        assertEquals(expResult, result);
    }

    /**
     * Test of numPersonagens method, of class RedeAliancas.
     */
    @Test
    public void testNumPersonagens() {
        
        System.out.println("numPersonagens");
        RedeAliancas instance = new RedeAliancas();
        int expResult1 = 0;
        int result1 = instance.numPersonagens();
        assertEquals(expResult1, result1);
        
        Personagem pers1 = new Personagem("p1", 20, new Local("local1", 20));
        Personagem pers2 = new Personagem("p2", 30, new Local("local2", 20));
        instance.adicionarPersonagem(pers1);
        instance.adicionarPersonagem(pers2);
        int expResult2 = 2;
        int result2 = instance.numPersonagens();
        
        assertEquals(expResult2, result2);
    }

    /**
     * Test of numAliancas method, of class RedeAliancas.
     */
    @Test
    public void testNumAliancas() {
        
        System.out.println("numAliancas");
        RedeAliancas instance = new RedeAliancas();
        int expResult = 0;
        int result = instance.numAliancas();
        assertEquals(expResult, result);
        
        Personagem pers1 = new Personagem("p1", 20, new Local("local1", 20));
        Personagem pers2 = new Personagem("p2", 30, new Local("local2", 20));
        instance.adicionarPersonagem(pers1);
        instance.adicionarPersonagem(pers2);
        instance.adicionarAlianca(true, 0.8f, pers1, pers2);
        int expResult2 = 2;
        int result2 = instance.numAliancas();
        
        assertEquals(expResult2, result2);
        
    }
    /**
     * Test of getPersonagemPeloNome method, of class RedeAliancas.
     */
    @Test
    public void testGetPersonagemPeloNome() {
        
        System.out.println("getPersonagemPeloNome");
        String nome = "p1";
        Local local1 = new Local("local1", 20);
        RedeAliancas instance = new RedeAliancas();
        Personagem expResult = new Personagem("p1", 50, local1);
        instance.adicionarPersonagem(expResult);
        
        Personagem result = instance.getPersonagemPeloNome(nome);
        
        assertEquals(expResult, result);
    }

    /**
     * Test of getAliancas method, of class RedeAliancas.
     */
    @Test
    public void testGetAliancas() {
        
        System.out.println("getAliancas");
        Personagem pers1 = new Personagem("p1", 20, new Local());
        Personagem pers2 = new Personagem("p2", 20, new Local());
        Personagem pers3 = new Personagem("p3", 20, new Local());
        Personagem pers4 = new Personagem("p4", 20, new Local());
        Personagem pers5 = new Personagem("p5", 20, new Local());
        
        RedeAliancas instance = new RedeAliancas();
        instance.adicionarPersonagem(pers1);
        instance.adicionarPersonagem(pers2);
        instance.adicionarPersonagem(pers3);
        instance.adicionarPersonagem(pers4);
        instance.adicionarPersonagem(pers5);
        instance.adicionarAlianca(true, 0.8f, pers1, pers2);
        instance.adicionarAlianca(true, 0.8f, pers3, pers4);
        instance.adicionarAlianca(true, 0.8f, pers3, pers5);
        
        List<Alianca> expResult = new LinkedList<>();
        expResult.add(new Alianca(true, 0.8f, pers1, pers2));
        expResult.add(new Alianca(true, 0.5f, pers3, pers4));
        expResult.add(new Alianca(true, 0.2f, pers3, pers5));
        
        List<Alianca> result = instance.getAliancas();
        
        assertEquals(expResult, result);
    }

    /**
     * Test of getAliadosAUmaPersonagem method, of class RedeAliancas.
     */
    @Test
    public void testGetAliadosAUmaPersonagem() {
        
        System.out.println("GetAliadosAUmaPersonagem");
        Personagem pers1 = new Personagem("p1", 20, new Local());
        Personagem pers2 = new Personagem("p2", 20, new Local());
        Personagem pers3 = new Personagem("p3", 20, new Local());
        
        RedeAliancas instance = new RedeAliancas();
        instance.adicionarPersonagem(pers1);
        instance.adicionarPersonagem(pers2);
        instance.adicionarPersonagem(pers3);
        instance.adicionarAlianca(true, 0.8f, pers1, pers2);
        instance.adicionarAlianca(true, 0.8f, pers1, pers3);
        
        List<Personagem> expResult = new LinkedList<>();
        expResult.add(pers2);
        expResult.add(pers3);
        
        List<Personagem> result = instance.getAliadosAUmaPersonagem(pers1);
        assertEquals(expResult, result);
        
    }

    /**
     * Test of irPeloNumMinimoAliancas method, of class RedeAliancas.
     */
    @Test
    public void testIrPeloNumMinimoAliancas() {
        
        System.out.println("numMinimoAliancas");
        RedeAliancas instance = new RedeAliancas();
        int expResult = 0;
        int result = instance.numAliancas();
        assertEquals(expResult, result);
        
        Personagem pers1 = new Personagem("p1", 20, new Local("local1", 20));
        Personagem pers2 = new Personagem("p2", 30, new Local("local2", 20));
        instance.adicionarPersonagem(pers1);
        instance.adicionarPersonagem(pers2);
        instance.adicionarAlianca(true, 0.8f, pers1, pers2);
        int expResult2 = 2;
        int result2 = instance.numAliancas();
        
        assertEquals(expResult2, result2);
    }

    /**
     * Test of localPertenceAliados method, of class RedeAliancas.
     */
    @Test
    public void testLocalPertenceAliados() {
        
        System.out.println("LocalPertenceAliados");
        Local local1 = new Local("local1", 20);
        Local local2 = new Local("local2", 20);
        Local local3 = new Local("local3", 20);
        
        Personagem pers1 = new Personagem("p1", 70, local1);
        Personagem pers2 = new Personagem("p2", 70, local2);
        
        local1.setP(pers1);
        local2.setP(pers2);
        
        RedeAliancas instance = new RedeAliancas();
        instance.adicionarPersonagem(pers1);
        instance.adicionarPersonagem(pers2);
        instance.adicionarAlianca(true, 0.8f, pers1, pers2);
        
        Boolean expResult = false;
        Boolean result = instance.localPertenceAliados(local3, pers1);
        assertEquals(expResult, result);
        
        expResult = true;
        result = instance.localPertenceAliados(local2, pers1);
        
        assertEquals(expResult, result);
    }

    /**
     * Test of keyPersonagem method, of class RedeAliancas.
     */
    @Test
    public void testKeyPersonagem() {
        
        System.out.println("keyPersonagem");
        Personagem pers1 = new Personagem("p1", 20, new Local());
        Personagem pers2 = new Personagem("p2", 20, new Local());
        Personagem pers3 = new Personagem("p3", 20, new Local());
        
        RedeAliancas instance = new RedeAliancas();
        instance.adicionarPersonagem(pers1);
        instance.adicionarPersonagem(pers2);
        instance.adicionarPersonagem(pers3);
        
        float expResult = 0.0F;
        float result = instance.keyPersonagem(pers1);
        assertEquals(expResult, result, 0.0);
        
        expResult = 1F;
        result = instance.keyPersonagem(pers2);
        assertEquals(expResult, result, 0.0);
        
        expResult = 2F;
        result = instance.keyPersonagem(pers3);
        
        assertEquals(expResult, result, 0.0);
        
    }

    /**
     * Test of personagemByKey method, of class RedeAliancas.
     */
    @Test
    public void testPersonagemByKey() {
        
        System.out.println("personagemByKey");
        int key = 0;
        
        RedeAliancas instance = new RedeAliancas();
        Personagem expResult = new Personagem("p1", 20, new Local());
        Personagem pers2 = new Personagem("p2", 20, new Local());
        instance.adicionarPersonagem(expResult);
        instance.adicionarPersonagem(pers2);
        
        Personagem result = instance.personagemByKey(key);
        
        assertEquals(expResult, result);
    }

    /**
     * Test of forcaPersonagens method, of class RedeAliancas.
     */
    @Test
    public void testForcaPersonagens() {
        
        System.out.println("forcaPersonagens");
        Personagem pers1 = new Personagem("p1", 20, new Local());
        Personagem pers2 = new Personagem("p2", 20, new Local());
        
        RedeAliancas instance = new RedeAliancas();
        instance.adicionarPersonagem(pers1);
        instance.adicionarPersonagem(pers2);
        instance.adicionarAlianca(true, 0.8f, pers1, pers2);
        
        Float expResult = 32f;
        Float result = instance.forcaPersonagens(pers1, pers2);
        assertEquals(expResult, result);
    }

    /**
     * Test of aliancasPersonagens method, of class RedeAliancas.
     */
    @Test
    public void testAliancasPersonagens() {
        
        System.out.println("AliancasPersonagens");
        Personagem pers1 = new Personagem("p1", 20, new Local());
        Personagem pers2 = new Personagem("p2", 20, new Local());
        Personagem pers3 = new Personagem("p3", 20, new Local());
        
        RedeAliancas instance = new RedeAliancas();
        instance.adicionarPersonagem(pers1);
        instance.adicionarPersonagem(pers2);
        instance.adicionarPersonagem(pers3);
        instance.adicionarAlianca(true, 0.8f, pers1, pers2);
        instance.adicionarAlianca(true, 0.5f, pers1, pers3);
        
        List<Personagem> expResult = new LinkedList<>();
        expResult.add(pers2);
        expResult.add(pers3);
        
        List<Personagem> result = instance.getAliadosAUmaPersonagem(pers1);
        
        assertEquals(expResult, result);
    }

    /**
     * Test of redeAliancasForcaMaxima method, of class RedeAliancas.
     */
    @Test
    public void testRedeAliancasForcaMaxima() {
        
        System.out.println("redeDeAliancasForcaMaxima");
        Personagem pers1 = new Personagem("p1", 20, new Local());
        Personagem pers2 = new Personagem("p2", 20, new Local());
        Personagem pers3 = new Personagem("p3", 40, new Local());
        Personagem pers4 = new Personagem("p4", 20, new Local());
         
        RedeAliancas instance = new RedeAliancas();
        instance.adicionarPersonagem(pers1);
        instance.adicionarPersonagem(pers2);
        instance.adicionarAlianca(true, 0.8f, pers1, pers2);
        instance.adicionarPersonagem(pers3);
        instance.adicionarPersonagem(pers4);
        instance.adicionarAlianca(true, 0.8f, pers1, pers2);
        
        List<Personagem> expPers = new LinkedList<>();
        expPers.add(pers3);
        expPers.add(pers4);
        
        boolean expResult = true;
        boolean result = expPers.contains(pers3);
        assertEquals(expResult, result);
        
        result = expPers.contains(pers4);
        assertEquals(expResult, result);
    }

    /**
     * Test of aliancasPublicas method, of class RedeAliancas.
     */
    @Test
    public void testAliancasPublicas() {
        
        System.out.println("AliancasPublicas");
        Personagem pers1 = new Personagem("p1", 20, new Local());
        Personagem pers2 = new Personagem("p2", 30, new Local());
        Personagem pers3 = new Personagem("p3", 40, new Local());
        Personagem pers4 = new Personagem("p4", 50, new Local());
        
        RedeAliancas instance = new RedeAliancas();
        instance.adicionarPersonagem(pers1);
        instance.adicionarPersonagem(pers2);
        instance.adicionarPersonagem(pers3);
        instance.adicionarPersonagem(pers4);
        instance.adicionarAlianca(true, 0.8f, pers1, pers2);
        instance.adicionarAlianca(true, 0.5f, pers2, pers3);
        instance.adicionarAlianca(true, 0.3f, pers1, pers4);
        
        Graph<Personagem, Alianca> g = new Graph<>(false);
        g.insertVertex(pers1);
        g.insertVertex(pers2);
        g.insertVertex(pers3);
        g.insertVertex(pers4);
        g.insertEdge(pers1, pers2, new Alianca(true, 0.8f, pers1, pers2), 0.8f);
        g.insertEdge(pers1, pers4, new Alianca(true, 0.3f, pers1, pers4), 0.3f);
        g.insertEdge(pers2, pers3, new Alianca(true, 0.5f, pers2, pers3), 0.5f);
       
        
        RedeAliancas expResult = null;
        expResult = instance.aliancasPublicas();
        expResult.adicionarAlianca(true, g.getEdge(pers1,pers2).getWeight(), pers1, pers3);
        expResult.adicionarAlianca(true, g.getEdge(pers3,pers4).getWeight(), pers3, pers4);
        expResult.adicionarAlianca(true, g.getEdge(pers2,pers4).getWeight(), pers2, pers4);
        
        RedeAliancas result = instance.aliancasPublicas();
        
        assertEquals(expResult, result);
    }

}
