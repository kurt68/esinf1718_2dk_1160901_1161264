/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.util.Objects;

/**
 *
 * @author user
 */
public class Alianca {

    private boolean tipo;
    private float fatorCompatibilidade;
    private Personagem p1;
    private Personagem p2;

    public Alianca(boolean tipo, float fatorCompatibilidade, Personagem p1, Personagem p2) {
        this.tipo = tipo;
        this.fatorCompatibilidade = fatorCompatibilidade;
        this.p1 = p1;
        this.p2 = p2;
    }

    public boolean isTipo() {
        return tipo;
    }

    public float getFatorCompatibilidade() {
        return fatorCompatibilidade;
    }

    public Personagem getP1() {
        return p1;
    }

    public Personagem getP2() {
        return p2;
    }

    public void setTipo(boolean tipo) {
        this.tipo = tipo;
    }

    public void setFatorCompatibilidade(float fatorCompatibilidade) {
        this.fatorCompatibilidade = fatorCompatibilidade;
    }

    public void setP1(Personagem p1) {
        this.p1 = p1;
    }

    public void setP2(Personagem p2) {
        this.p2 = p2;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 37 * hash + (this.tipo ? 1 : 0);
        hash = 37 * hash + Float.floatToIntBits(this.fatorCompatibilidade);
        hash = 37 * hash + Objects.hashCode(this.p1);
        hash = 37 * hash + Objects.hashCode(this.p2);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Alianca other = (Alianca) obj;
        if (this.tipo != other.tipo) {
            return false;
        }
        if (Float.floatToIntBits(this.fatorCompatibilidade) != Float.floatToIntBits(other.fatorCompatibilidade)) {
            return false;
        }
        if (!Objects.equals(this.p1, other.p1)) {
            return false;
        }
        if (!Objects.equals(this.p2, other.p2)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Alianca{" + "tipo=" + tipo + ", fatorCompatibilidade=" + fatorCompatibilidade + ", p1=" + p1 + ", p2=" + p2 + '}';
    }
}
